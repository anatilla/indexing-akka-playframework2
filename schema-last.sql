--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.7
-- Dumped by pg_dump version 9.4.2
-- Started on 2015-06-09 18:02:11 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE indexer;
--
-- TOC entry 1997 (class 1262 OID 12035)
-- Name: indexer; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE indexer WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


\connect indexer

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1998 (class 1262 OID 12035)
-- Dependencies: 1997
-- Name: indexer; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON DATABASE indexer IS 'default administrative connection database';


--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA public;


--
-- TOC entry 1999 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 179 (class 3079 OID 11756)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2000 (class 0 OID 0)
-- Dependencies: 179
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_with_oids = false;

--
-- TOC entry 170 (class 1259 OID 16392)
-- Name: corpora; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE corpora (
    corpus_id character varying NOT NULL,
    weight_name character varying
);


--
-- TOC entry 171 (class 1259 OID 16398)
-- Name: documents; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE documents (
    document_id character varying NOT NULL,
    corpus_id character varying NOT NULL,
    id bigint NOT NULL
);


--
-- TOC entry 176 (class 1259 OID 16490)
-- Name: documents_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2001 (class 0 OID 0)
-- Dependencies: 176
-- Name: documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE documents_id_seq OWNED BY documents.id;


--
-- TOC entry 172 (class 1259 OID 16404)
-- Name: models; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE models (
    model_id character varying NOT NULL,
    corpus_id character varying,
    description text,
    algorithm character varying,
    label_field character varying,
    train_field character varying,
    param_json text
);


--
-- TOC entry 173 (class 1259 OID 16410)
-- Name: splits; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE splits (
    split_id character varying NOT NULL,
    corpus_id character varying,
    seed bigint
);


--
-- TOC entry 174 (class 1259 OID 16416)
-- Name: testsets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE testsets (
    doc_id bigint,
    split character varying,
    id bigint NOT NULL
);


--
-- TOC entry 177 (class 1259 OID 16535)
-- Name: testsets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE testsets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2002 (class 0 OID 0)
-- Dependencies: 177
-- Name: testsets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE testsets_id_seq OWNED BY testsets.id;


--
-- TOC entry 175 (class 1259 OID 16424)
-- Name: trainsets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE trainsets (
    split character varying,
    doc_id bigint,
    id bigint NOT NULL
);


--
-- TOC entry 178 (class 1259 OID 16547)
-- Name: trainsets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE trainsets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2003 (class 0 OID 0)
-- Dependencies: 178
-- Name: trainsets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE trainsets_id_seq OWNED BY trainsets.id;


--
-- TOC entry 1857 (class 2604 OID 16492)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY documents ALTER COLUMN id SET DEFAULT nextval('documents_id_seq'::regclass);


--
-- TOC entry 1858 (class 2604 OID 16537)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY testsets ALTER COLUMN id SET DEFAULT nextval('testsets_id_seq'::regclass);


--
-- TOC entry 1859 (class 2604 OID 16549)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY trainsets ALTER COLUMN id SET DEFAULT nextval('trainsets_id_seq'::regclass);


--
-- TOC entry 1861 (class 2606 OID 16432)
-- Name: pk_corpora_corpusid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY corpora
    ADD CONSTRAINT pk_corpora_corpusid PRIMARY KEY (corpus_id);


--
-- TOC entry 1864 (class 2606 OID 16501)
-- Name: pk_documents; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT pk_documents PRIMARY KEY (id);


--
-- TOC entry 1868 (class 2606 OID 16436)
-- Name: pk_modelid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY models
    ADD CONSTRAINT pk_modelid PRIMARY KEY (model_id);


--
-- TOC entry 1870 (class 2606 OID 16438)
-- Name: pk_splits; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY splits
    ADD CONSTRAINT pk_splits PRIMARY KEY (split_id);


--
-- TOC entry 1872 (class 2606 OID 16546)
-- Name: pk_testset; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY testsets
    ADD CONSTRAINT pk_testset PRIMARY KEY (id);


--
-- TOC entry 1876 (class 2606 OID 16558)
-- Name: pk_trainset; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trainsets
    ADD CONSTRAINT pk_trainset PRIMARY KEY (id);


--
-- TOC entry 1866 (class 2606 OID 16444)
-- Name: uq_docs; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT uq_docs UNIQUE (document_id, corpus_id);


--
-- TOC entry 1874 (class 2606 OID 16527)
-- Name: uq_testset; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY testsets
    ADD CONSTRAINT uq_testset UNIQUE (doc_id, split);


--
-- TOC entry 1878 (class 2606 OID 16534)
-- Name: uq_trainset; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trainsets
    ADD CONSTRAINT uq_trainset UNIQUE (split, doc_id);


--
-- TOC entry 1862 (class 1259 OID 16512)
-- Name: idx_hash_docid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_hash_docid ON documents USING hash (document_id);


--
-- TOC entry 1881 (class 2606 OID 16590)
-- Name: fk_coprus; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY splits
    ADD CONSTRAINT fk_coprus FOREIGN KEY (corpus_id) REFERENCES corpora(corpus_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1879 (class 2606 OID 16600)
-- Name: fk_corpus; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT fk_corpus FOREIGN KEY (corpus_id) REFERENCES corpora(corpus_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1880 (class 2606 OID 16595)
-- Name: fk_corpus_corpora; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY models
    ADD CONSTRAINT fk_corpus_corpora FOREIGN KEY (corpus_id) REFERENCES corpora(corpus_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1882 (class 2606 OID 16580)
-- Name: fk_docid_documents; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY testsets
    ADD CONSTRAINT fk_docid_documents FOREIGN KEY (doc_id) REFERENCES documents(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1885 (class 2606 OID 16575)
-- Name: fk_docs_documents; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trainsets
    ADD CONSTRAINT fk_docs_documents FOREIGN KEY (doc_id) REFERENCES documents(id);


--
-- TOC entry 1883 (class 2606 OID 16585)
-- Name: fk_split_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY testsets
    ADD CONSTRAINT fk_split_id FOREIGN KEY (split) REFERENCES splits(split_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1884 (class 2606 OID 16570)
-- Name: fk_split_splits; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trainsets
    ADD CONSTRAINT fk_split_splits FOREIGN KEY (split) REFERENCES splits(split_id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2015-06-09 18:02:18 CEST

--
-- PostgreSQL database dump complete
--

