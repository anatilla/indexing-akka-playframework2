
import akka.actor.Props;
import it.alessandronatilla.analysis.ml.AlgorithmManager;
import it.alessandronatilla.analysis.ml.algorithms.DecisionTreeAlgorithm;
import it.alessandronatilla.analysis.ml.algorithms.LogisticRegressionAlgorithm;
import it.alessandronatilla.analysis.ml.algorithms.NaiveBayesAlgorithm;
import it.alessandronatilla.analysis.ml.algorithms.RandomForestAlgorithm;
import it.alessandronatilla.analysis.spark.SparkProvider;
import it.alessandronatilla.indexing.actors.MasterActor;
import it.alessandronatilla.preprocessing.OpenNLPTagger;
import it.alessandronatilla.preprocessing.OpenNLPTokenizer;
import it.alessandronatilla.preprocessing.lemmatizer.it.LemmatizerITSingleton;
import it.alessandronatilla.utils.JLogger;
import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.stores.datastore.DataStoreManager;
import it.alessandronatilla.stores.modelstore.ModelStore;
import it.alessandronatilla.stores.splitstore.SplitStore;
import play.Application;
import play.GlobalSettings;
import play.libs.Akka;
import play.libs.F.*;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
/**
 * Created by alexander on 07/05/15.
 */
public class Global extends GlobalSettings {

	 // For CORS
  private class ActionWrapper extends Action.Simple {
    public ActionWrapper(Action<?> action) {
      this.delegate = action;
    }

    @Override
    public Promise<Result> call(Http.Context ctx) throws java.lang.Throwable {
      Promise<Result> result = this.delegate.call(ctx);
      Http.Response response = ctx.response();
      response.setHeader("Access-Control-Allow-Origin", "*");
      return result;
    }
  }

  @Override
  public Action<?> onRequest(Http.Request request,
      java.lang.reflect.Method actionMethod) {
    return new ActionWrapper(super.onRequest(request, actionMethod));
  }

    @Override
    public void onStart(Application application) {
        //initializing heavy components
        OpenNLPTagger.initialize();
        OpenNLPTokenizer.initialize();
        LemmatizerITSingleton.getInstance();
        SparkProvider.getInstance();

        AlgorithmManager algorithmManager = AlgorithmManager.getInstance();
        algorithmManager.registerAlgorithm("decision_trees", new DecisionTreeAlgorithm());
        algorithmManager.registerAlgorithm("random_forest", new RandomForestAlgorithm());
        algorithmManager.registerAlgorithm("naive_bayes", new NaiveBayesAlgorithm());
        algorithmManager.registerAlgorithm("logistic_regression", new LogisticRegressionAlgorithm());
    }


    @Override
    public void onStop(Application application) {

        JLogger.getLogger(this.getClass().getName()).info("Requested shutdown........");
        DataStoreManager manager = DataStoreManager.getInstance();
        for (DataStore store : manager) {
            JLogger.getLogger(this.getClass().getName()).info("Finalized index: " + store.getIndexId());
            store.finalize();
        }
        JLogger.getLogger(this.getClass().getName()).info("All indexes finalized.");

        /*Akka.system().awaitTermination(Duration.createAdHocSplit(15, TimeUnit.SECONDS));*/
        Akka.system().shutdown();
        JLogger.getLogger(this.getClass().getName()).info("System shutdown. bye :)");

        SplitStore.finalizeInstance();
        DataStoreManager.finalizeInstance();
        ModelStore.finalizeInstance();
    }
}
