package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.alessandronatilla.utils.JLogger;
import play.libs.Json;

/**
 * Created by alexander on 14/05/15.
 */
public class JsonUtils {
    public static String json_prettify(JsonNode jsonNode) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode json = Json.toJson(jsonNode);
        String pretty = "";
        try {
            pretty = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
        } catch (JsonProcessingException e) {
            JLogger.getLogger(JsonUtils.class).error(e.toString());
        }
        return pretty;
    }
}
