package controllers;

import it.alessandronatilla.analysis.model.split.AdHocSplitProperty;
import it.alessandronatilla.analysis.model.split.RandomSplitProperty;
import it.alessandronatilla.analysis.rest.services.splits.*;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.Map;

/**
 * Created by alexander on 16/05/15.
 */
public class SplitController extends Controller {

    /**
     * @return
     */
    @Transactional
    @BodyParser.Of(value = BodyParser.Json.class)
    public static Result createAdHocSplit() {
        AdHocSplitProperty property = Json.fromJson(request().body().asJson(), AdHocSplitProperty.class);
        return ok(JsonUtils.json_prettify(Json.toJson(CreateAdHocSplitService.getData(property))));

    }

    /**
     * @return
     */
    @Transactional
    @BodyParser.Of(value = BodyParser.Json.class)
    public static Result createRandomSplit() {
        RandomSplitProperty property = Json.fromJson(request().body().asJson(), RandomSplitProperty.class);
        return ok(JsonUtils.json_prettify(Json.toJson(CreateRandomSplitService.getData(property))));
    }

    /**
     * @param splitId
     * @param corpusId
     * @return
     */
    @Transactional
    public static Result get(String splitId, String corpusId) {
        Map result = FindSplitService.getData(splitId, corpusId);
        return ok(JsonUtils.json_prettify(Json.toJson(result)));
    }

    /**
     * @param splitId
     * @param corpusId
     * @return
     */
    @Transactional
    public static Result delete(String splitId, String corpusId) {
        Map results = DeleteSplitService.getData(splitId, corpusId);
        return ok(JsonUtils.json_prettify(Json.toJson(results)));
    }

    /**
     * @return
     */
    @Transactional
    public static Result listSplits() {
        Map results = ListSplitService.getData();
        return ok(JsonUtils.json_prettify(Json.toJson(results)));
    }

}
