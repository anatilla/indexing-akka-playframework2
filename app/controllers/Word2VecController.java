package controllers;

import it.alessandronatilla.analysis.model.matrix.IndexedField;
import it.alessandronatilla.analysis.word2vec.SemanticVectorsW2V;
import it.alessandronatilla.analysis.word2vec.SparkW2V;
import it.alessandronatilla.analysis.word2vec.W2VResult;
import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.stores.datastore.DataStoreManager;
import it.alessandronatilla.stores.datastore.TransactionalDelegate;
import it.alessandronatilla.utils.JLogger;
import it.alessandronatilla.utils.TimeProvider;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

/**
 * Created by alexander on 06/06/15.
 */
public class Word2VecController extends Controller {

    @Transactional
    public static Result word2vec(String engine, String corpusId, String term, String s_field) {
        JLogger.getLogger(MLController.class).info("Term is :" + term + ":");
        DataStoreManager dataStoreManager = DataStoreManager.getInstance();
        DataStore store = dataStoreManager.get(corpusId);
        Map<String, Object> result = new TreeMap<>();
        IndexedField field = IndexedField.valueOf(s_field);

        store.create(new TransactionalDelegate() {
            private List<W2VResult> couples;
            private Long elapsed;

            @Override
            public void onExecute(DataStore.Accessor t) throws Exception {

                TimeProvider timeProvider = TimeProvider.getInstance();
                String session = engine + ":" + corpusId + ":" + term + ":" + s_field + ":" + UUID.randomUUID().toString();

                timeProvider.create(session);
                timeProvider.start(session);

                if (engine.equalsIgnoreCase("spark")) {
                    SparkW2V wrapper = new SparkW2V(t, field);
                    couples = wrapper.query(term, 20);
                } else if (engine.equalsIgnoreCase("semanticvectors")) {
                    SemanticVectorsW2V sv = new SemanticVectorsW2V(t, field);
                    couples = sv.query(term);

                    // cancellazione necessaria per evitare di incrementare i modelli e sballare gli score.
                    sv.deleteModels();
                }

                elapsed = timeProvider.getElapsed(session);
            }

            @Override
            public void onFailure(Exception e) {
                JLogger.getLogger(this).error(e.getMessage());
                e.printStackTrace();
                result.put("status", "error");
                result.put("details", e.getMessage());
            }

            @Override
            public void onSuccess() {
                result.put("status", "ok");
                result.put("results for", term);
                result.put("details", couples);
                result.put("elapsed", elapsed);

            }
        }).dispatch();

        return ok(JsonUtils.json_prettify(Json.toJson(result)));
    }

    @Transactional
    public static Result word2doc(String corpusId, String term, String s_field) {
        JLogger.getLogger(MLController.class).info("Term is :" + term + ":");
        DataStoreManager dataStoreManager = DataStoreManager.getInstance();
        DataStore store = dataStoreManager.get(corpusId);
        Map<String, Object> result = new TreeMap<>();
        IndexedField field = IndexedField.valueOf(s_field);

        store.create(new TransactionalDelegate() {
            private List<W2VResult> couples;
            private Long elapsed;

            @Override
            public void onExecute(DataStore.Accessor t) throws Exception {

                TimeProvider timeProvider = TimeProvider.getInstance();
                String session = corpusId + ":" + term + ":" + s_field + ":" + UUID.randomUUID().toString();

                timeProvider.create(session);
                timeProvider.start(session);

                SemanticVectorsW2V sv = new SemanticVectorsW2V(t, field);
                couples = sv.queryDocs(term);

                // cancellazione necessaria per evitare di incrementare i modelli e sballare gli score.
                sv.deleteModels();

                elapsed = timeProvider.getElapsed(session);
            }

            @Override
            public void onFailure(Exception e) {
                JLogger.getLogger(this).error(e.getMessage());
                e.printStackTrace();
                result.put("status", "error");
                result.put("details", e.getMessage());
            }

            @Override
            public void onSuccess() {
                result.put("status", "ok");
                result.put("results for", term);
                result.put("details", couples);
                result.put("elapsed", elapsed);

            }
        }).dispatch();

        return ok(JsonUtils.json_prettify(Json.toJson(result)));
    }
}
