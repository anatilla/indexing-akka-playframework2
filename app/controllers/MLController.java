package controllers;


import com.fasterxml.jackson.databind.JsonNode;
import it.alessandronatilla.analysis.exceptions.AlgorithmNotFoundException;
import it.alessandronatilla.analysis.ml.Algorithm;
import it.alessandronatilla.analysis.ml.AlgorithmManager;
import it.alessandronatilla.analysis.ml.ClassificationModel;
import it.alessandronatilla.analysis.model.matrix.IndexedField;
import it.alessandronatilla.analysis.model.ml.TestProperty;
import it.alessandronatilla.analysis.model.ml.TrainProperty;
import it.alessandronatilla.analysis.rest.services.algorithms.ListAvailableAlgorithmService;
import it.alessandronatilla.analysis.rest.services.models.DeleteModelService;
import it.alessandronatilla.analysis.rest.services.models.ListMLModelsService;
import it.alessandronatilla.analysis.rest.services.models.TestModelService;
import it.alessandronatilla.analysis.rest.services.models.TrainModelService;
import it.alessandronatilla.analysis.word2vec.SparkW2V;
import it.alessandronatilla.analysis.word2vec.W2VResult;
import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.stores.datastore.DataStoreManager;
import it.alessandronatilla.stores.datastore.TransactionalDelegate;
import it.alessandronatilla.utils.JLogger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by alexander on 07/05/15.
 */
public class MLController extends Controller {
    public static Result listAlgorithms() {
        Object result = ListAvailableAlgorithmService.getData();
        return ok(JsonUtils.json_prettify(Json.toJson(result)));
    }

    @Transactional
    public static Result deleteModel(String modelId) {
        return ok(JsonUtils.json_prettify(Json.toJson(DeleteModelService.getData(modelId))));
    }

    @BodyParser.Of(BodyParser.Json.class)
    @Transactional
    public static Result train() {
        /*
         * testModel json object
    	 * 
    	 * {
			   "globalParam":{
			      "modelId":"reuters-dt-115",
			      "description":"nb 1",
			      "algorithmName":"decision_trees",
			      "corpusId":"reuters",
			      "labelAttribute":"label",
			      "startTrainId":0,
			      "endTrainId":1000
			   },
			   "algorithmParam":{
			      "classCount":3,
			      "maxDepth":10,
			      "bins":4,
			      "impurityMeasure":"gini"
			   }
			}
    	 */


        try {
            JsonNode globalParams = request().body().asJson().get("globalParam");
            JsonNode algoParams = request().body().asJson().get("algorithmParam");

            TrainProperty props = Json.fromJson(globalParams, TrainProperty.class);


            AlgorithmManager manager = AlgorithmManager.getInstance();
            Serializable properties;
            try {
                Algorithm algorithmImpl = manager.getAlgorithm(props.getAlgorithmName().toString());
                Class proxyObject = algorithmImpl.getPojoParamClass();
                properties = (Serializable) Json.fromJson(algoParams, proxyObject);
                ClassificationModel model = algorithmImpl.buildClassifier(properties);

                TrainModelService service = new TrainModelService();
                Map results = service.trainModel(props, model);
                return ok(JsonUtils.json_prettify(Json.toJson(results)));


            } catch (AlgorithmNotFoundException e) {
                JLogger.getLogger(MLController.class.getName()).error(e.getMessage());
                return badRequest("algorithm " + props.getAlgorithmName().toString() + " not registered");
            }
        } catch (NullPointerException ex) {
            JLogger.getLogger(MLController.class.getName()).error(ex.getMessage());
            return badRequest("incorrect parameters.");
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    @Transactional
    public static Result test() {
        TestProperty properties = Json.fromJson(request().body().asJson(), TestProperty.class);
        Map results = new TestModelService().testModel(properties);
        return ok(JsonUtils.json_prettify(Json.toJson(results)));
    }

    @Transactional
    public static Result enumerate_models() {
        Object results = ListMLModelsService.getResult();
        return ok(JsonUtils.json_prettify(Json.toJson(results)));
    }


}
