package controllers;

import akka.actor.Props;
import it.alessandronatilla.indexing.actors.MasterActor;
import it.alessandronatilla.indexing.rest.model.CorpusObject;
import it.alessandronatilla.indexing.rest.model.IndexProperties;
import it.alessandronatilla.indexing.rest.services.*;
import it.alessandronatilla.properties.PropertiesLoader;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Akka;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class IndexManagementController extends Controller {

    private static int NUM_ACTORS;

    static {
        try {
            NUM_ACTORS = Integer.parseInt(PropertiesLoader.load().getProperty("index.numactors"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Transactional(readOnly = true)
    public static Result print() {
        return ok(JsonUtils.json_prettify(Json.toJson(GetHashMap.print())));
    }

    @BodyParser.Of(BodyParser.Json.class)
    @Transactional
    public static Result create() {
        IndexProperties props = Json.fromJson(request().body().asJson(), IndexProperties.class);
        Map result = IndexCreationService.getData(props);
        return ok(JsonUtils.json_prettify(Json.toJson(result)));
    }


    @BodyParser.Of(value = BodyParser.Json.class, maxLength = 50 * 1024 * 1024) //max data size: 50MB
    @Transactional
    public static Result indexContent() {
        if (request().body().isMaxSizeExceeded()) {
            return badRequest("Too much data!");
        }

        CorpusObject corpusObject = Json.fromJson(request().body().asJson(), CorpusObject.class);
        String uuid = UUID.randomUUID().toString();
        Akka.system().actorOf(Props.create(MasterActor.class, NUM_ACTORS), uuid);

        Object final_res = new IndexContentService().getData(corpusObject, uuid);
        return ok(Json.toJson(final_res));
    }

    public static Result deleteIndex(String indexID) {
        Map result = null;
        try {
            result = JPA.withTransaction(() -> IndexDeletionService.getData(indexID));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return ok(JsonUtils.json_prettify(Json.toJson(result)));
    }

    @Transactional(readOnly = true)
    public static Result listIndices() {
        List<String> indices = IndexEnumeratorService.getData();
        return ok(JsonUtils.json_prettify(Json.toJson(indices)));
    }

}
