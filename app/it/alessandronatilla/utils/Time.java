package it.alessandronatilla.utils;

/**
 * Author: alexander
 * Project: crowd-pulse
 */

import java.util.concurrent.TimeUnit;

class Time {

    private long start;
    private long end;

    public Time() {
        this.start = 0l;
        this.end = 0l;
    }

    public void start() {
        start = System.nanoTime();
    }

    public void end() {
        end = System.nanoTime();
    }

    public long elapsedTimeMilliseconds() {
        return TimeUnit.MILLISECONDS.convert((end - start), TimeUnit.NANOSECONDS);
    }
}

