package it.alessandronatilla.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexander on 18/05/15.
 */
public class TimeProvider {

    private static TimeProvider instance;

    private Map<String, Time> sessionTime;

    public static TimeProvider getInstance() {
        if (instance == null) {
            instance = new TimeProvider();
        }
        return instance;
    }

    private TimeProvider() {
        this.sessionTime = new HashMap<>();
    }

    public void create(String session) throws IllegalArgumentException {
        if (this.sessionTime.containsKey(session))
            throw new IllegalArgumentException(session + " already exists.");
        this.sessionTime.put(session, new Time());
    }

    public void start(String session) throws IllegalArgumentException {
        if (!this.sessionTime.containsKey(session))
            throw new IllegalArgumentException("no session found: " + session);
        this.sessionTime.get(session).start();
    }


    public Long getElapsed(String session) throws IllegalArgumentException {
        if (!this.sessionTime.containsKey(session))
            throw new IllegalArgumentException("no session found: " + session);

        Time time = this.sessionTime.get(session);
        time.end();
        return time.elapsedTimeMilliseconds();
    }

    public void reset(String session) throws IllegalArgumentException {
        if (!this.sessionTime.containsKey(session))
            throw new IllegalArgumentException("no session found: " + session);
        this.sessionTime.remove(session);
        this.sessionTime.put(session, new Time());
    }
}
