package it.alessandronatilla.utils;

/**
 * Author: alexander
 * Project: text-preprocessor
 */

import play.Logger;



public class JLogger {


    public static Logger.ALogger getLogger(Class clazz) {
        return Logger.of(clazz);
    }

    public static Logger.ALogger getLogger(String name) {
        return Logger.of(name);
    }

    public static Logger.ALogger getLogger(Object obj) {
        return getLogger(obj.getClass().getName());
    }

    public static org.slf4j.Logger toLogger(Logger.ALogger logger){
        return logger.underlying();
    }



}

