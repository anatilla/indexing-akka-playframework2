package it.alessandronatilla.stores.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

/**
 * Created by alexander on 23/05/15.
 */
@Entity
@Table(name = "splits")
@NamedQueries({
        @NamedQuery(name = "SplitsEntity.findByCorpus",
                query = "SELECT d FROM SplitsEntity d WHERE d.corpusId = :corpusId")
})
public class SplitsEntity {
    private String splitId;
    private String corpusId;
    private Long seed;

    public SplitsEntity() {
    }

    public SplitsEntity(String corpusId, Long seed, String splitId) {
        this.corpusId = corpusId;
        this.seed = seed;
        this.splitId = splitId;
    }

    @Id
    @Column(name = "split_id", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getSplitId() {
        return splitId;
    }

    public void setSplitId(String splitId) {
        this.splitId = splitId;
    }

    @Basic
    @Column(name = "corpus_id", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getCorpusId() {
        return corpusId;
    }

    public void setCorpusId(String corpusId) {
        this.corpusId = corpusId;
    }

    @Basic
    @Column(name = "seed", nullable = true, insertable = true, updatable = true)
    public Long getSeed() {
        return seed;
    }

    public void setSeed(Long seed) {
        this.seed = seed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SplitsEntity that = (SplitsEntity) o;
        return Objects.equals(splitId, that.splitId) &&
                Objects.equals(corpusId, that.corpusId) &&
                Objects.equals(seed, that.seed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(splitId, corpusId, seed);
    }
}
