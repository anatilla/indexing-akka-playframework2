package it.alessandronatilla.stores.model;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by alexander on 23/05/15.
 */
@Entity
@Table(name = "documents")
@NamedQueries({
        @NamedQuery(name = "DocumentsEntity.findByCorpusAndId",
                query = "SELECT d FROM DocumentsEntity d WHERE d.corpus.corpusId = :corpusId AND d.documentId = :dId"),
        @NamedQuery(name = "DocumentsEntity.findByCorpus",
                query = "SELECT d FROM DocumentsEntity d WHERE d.corpus.corpusId = :corpusId")

})
public class DocumentsEntity {
    private String documentId;
    private long id;
    private CorporaEntity corpus;

    public DocumentsEntity() {
    }

    public DocumentsEntity(CorporaEntity corpus, String documentId) {
        this.corpus = corpus;
        this.documentId = documentId;
    }

    @Basic
    @Column(name = "document_id", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DocumentsEntity that = (DocumentsEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(documentId, that.documentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(documentId, id);
    }

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "corpus_id", referencedColumnName = "corpus_id", nullable = false)
    public CorporaEntity getCorpus() {
        return corpus;
    }

    public void setCorpus(CorporaEntity corporaByCorpusId) {
        this.corpus = corporaByCorpusId;
    }
}
