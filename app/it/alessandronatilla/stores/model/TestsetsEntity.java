package it.alessandronatilla.stores.model;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by alexander on 23/05/15.
 */
@Entity
@Table(name = "testsets")
@NamedQueries({
        @NamedQuery(name = "TestsetsEntity.getDocuments",
                query = "SELECT d.document FROM TestsetsEntity d WHERE d.split = :splitId")
})
public class TestsetsEntity {
    private long id;
    private DocumentsEntity document;
    private SplitsEntity split;

    public TestsetsEntity(DocumentsEntity document, SplitsEntity split) {
        this.document = document;
        this.split = split;
    }

    public TestsetsEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestsetsEntity that = (TestsetsEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "doc_id", referencedColumnName = "id")
    public DocumentsEntity getDocument() {
        return document;
    }

    public void setDocument(DocumentsEntity documentsByDocId) {
        this.document = documentsByDocId;
    }

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "split", referencedColumnName = "split_id")
    public SplitsEntity getSplit() {
        return split;
    }

    public void setSplit(SplitsEntity splitsBySplit) {
        this.split = splitsBySplit;
    }
}
