package it.alessandronatilla.stores.model;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by alexander on 23/05/15.
 */
@Entity
@Table(name = "models")
@NamedQueries({
        @NamedQuery(name = "ModelsEntity.findAll",
                query = "SELECT d FROM ModelsEntity d"),
        @NamedQuery(name = "ModelsEntity.findByCorpus",
                query = "SELECT m FROM ModelsEntity m where m.corpus = :corpus")

}
)
public class ModelsEntity {
    private String modelId;
    private String description;
    private String algorithm;
    private String labelField;
    private String trainField;
    private String paramJson;
    private CorporaEntity corpus;

    public ModelsEntity() {
    }

    public ModelsEntity(String modelAlgorithmName, CorporaEntity corpus, String modelDescription, String labelAttribute, String modelID, String trainField, String params) {
        this.algorithm = modelAlgorithmName;
        this.corpus = corpus;
        this.description = modelDescription;
        this.labelField = labelAttribute;
        this.modelId = modelID;
        this.trainField = trainField;
        this.paramJson = params;
    }

    @Id
    @Column(name = "model_id", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    @Basic
    @Column(name = "description", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "algorithm", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    @Basic
    @Column(name = "label_field", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getLabelField() {
        return labelField;
    }

    public void setLabelField(String labelField) {
        this.labelField = labelField;
    }

    @Basic
    @Column(name = "train_field", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getTrainField() {
        return trainField;
    }

    public void setTrainField(String trainField) {
        this.trainField = trainField;
    }

    @Basic
    @Column(name = "param_json", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getParamJson() {
        return paramJson;
    }

    public void setParamJson(String paramJson) {
        this.paramJson = paramJson;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelsEntity entity = (ModelsEntity) o;
        return Objects.equals(modelId, entity.modelId) &&
                Objects.equals(description, entity.description) &&
                Objects.equals(algorithm, entity.algorithm) &&
                Objects.equals(labelField, entity.labelField) &&
                Objects.equals(trainField, entity.trainField) &&
                Objects.equals(paramJson, entity.paramJson);
    }

    @Override
    public int hashCode() {
        return Objects.hash(modelId, description, algorithm, labelField, trainField, paramJson);
    }

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "corpus_id", referencedColumnName = "corpus_id", nullable = false)
    public CorporaEntity getCorpus() {
        return corpus;
    }

    public void setCorpus(CorporaEntity corporaByCorpusId) {
        this.corpus = corporaByCorpusId;
    }
}
