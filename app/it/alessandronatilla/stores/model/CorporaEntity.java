package it.alessandronatilla.stores.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

/**
 * Created by alexander on 23/05/15.
 */
@Entity
@Table(name = "corpora")
@NamedQueries({
        @NamedQuery(name = "CorporaEntity.findAll",
                query = "SELECT c FROM CorporaEntity c")
})
public class CorporaEntity {
    private String corpusId;
    private String weightName;

    public CorporaEntity() {
    }

    public CorporaEntity(String corpusId, String weightName) {
        this.corpusId = corpusId;
        this.weightName = weightName;
    }

    @Id
    @Column(name = "corpus_id", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getCorpusId() {
        return corpusId;
    }

    public void setCorpusId(String corpusId) {
        this.corpusId = corpusId;
    }

    @Basic
    @Column(name = "weight_name", nullable = true, insertable = true, updatable = true, length = 2147483647)
    public String getWeightName() {
        return weightName;
    }

    public void setWeightName(String weightName) {
        this.weightName = weightName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CorporaEntity that = (CorporaEntity) o;
        return Objects.equals(corpusId, that.corpusId) &&
                Objects.equals(weightName, that.weightName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(corpusId, weightName);
    }
}
