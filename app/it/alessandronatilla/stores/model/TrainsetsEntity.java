package it.alessandronatilla.stores.model;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by alexander on 23/05/15.
 */
@Entity
@Table(name = "trainsets")
@NamedQueries({
        @NamedQuery(name = "TrainSets.getDocuments",
                query = "SELECT d.document FROM TrainsetsEntity d WHERE d.split = :splitId")
})
public class TrainsetsEntity {
    private long id;
    private DocumentsEntity document;
    private SplitsEntity split;

    public TrainsetsEntity(DocumentsEntity document, SplitsEntity split) {
        this.document = document;
        this.split = split;
    }

    public TrainsetsEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrainsetsEntity that = (TrainsetsEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "doc_id", referencedColumnName = "id")
    public DocumentsEntity getDocument() {
        return document;
    }

    public void setDocument(DocumentsEntity documentsByDocId) {
        this.document = documentsByDocId;
    }

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "split", referencedColumnName = "split_id")
    public SplitsEntity getSplit() {
        return split;
    }

    public void setSplit(SplitsEntity splitsBySplit) {
        this.split = splitsBySplit;
    }
}
