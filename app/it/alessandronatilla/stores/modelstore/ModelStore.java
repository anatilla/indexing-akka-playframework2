package it.alessandronatilla.stores.modelstore;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.alessandronatilla.analysis.exceptions.AlgorithmNotFoundException;
import it.alessandronatilla.analysis.ml.Algorithm;
import it.alessandronatilla.analysis.ml.AlgorithmManager;
import it.alessandronatilla.analysis.ml.ClassificationModel;
import it.alessandronatilla.properties.PropertiesLoader;
import it.alessandronatilla.stores.dao.CorporaDAO;
import it.alessandronatilla.stores.dao.ModelsDao;
import it.alessandronatilla.stores.model.CorporaEntity;
import it.alessandronatilla.stores.model.ModelsEntity;
import it.alessandronatilla.utils.JLogger;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Stream;

public class ModelStore {

    private static ModelStore instance;

    /**
     * Catalogo generale
     */
    ObjectMapper json_mapper;

    private ModelStore() {
        json_mapper = new ObjectMapper();
    }

    public static ModelStore getInstance() {
        if (instance == null) {
            instance = new ModelStore();
        }
        return instance;
    }

    public static void finalizeInstance() {
        if (instance != null) {
            instance = null;
        }
    }


    public void delete(String modelId) throws IOException {
        ModelsDao dao = new ModelsDao();
        ModelsEntity entity = null;
        try {
            entity = dao.findById(modelId);

            if (entity != null) {
                FileUtils.deleteDirectory(new File(this.getModelPath(entity.getCorpus().getCorpusId(), entity.getModelId())));
                dao.remove(modelId);
            }
        } catch (Exception e) {
            JLogger.getLogger(this).error(e.getMessage());
        }


    }

    public ClassificationModel getModel(String modelId) {
        ModelsDao dao = new ModelsDao();
        ModelsEntity entity = null;
        ClassificationModel model = null;

        try {
            if (this.hasModel(modelId)) {
                AlgorithmManager manager = AlgorithmManager.getInstance();
                entity = dao.findById(modelId);

                Algorithm<Serializable> algo = (Algorithm<Serializable>) manager.getAlgorithm(entity.getAlgorithm());
                JsonNode json = json_mapper.readTree(entity.getParamJson());
                Serializable param = play.libs.Json.fromJson(json, algo.getPojoParamClass());
                String modelPath = this.getModelPath(entity.getCorpus().getCorpusId(), modelId);

                model = algo.buildClassifier(param);
                model.load(modelPath);
            }
        } catch (IOException e) {
            JLogger.getLogger(this).error(e.getMessage());
            model = null;
        } catch (AlgorithmNotFoundException e) {
            JLogger.getLogger(this).error(e.getMessage());
            model = null;
        } catch (Exception e) {
            JLogger.getLogger(this).error(e.getMessage());
            model = null;
        }

        return model;
    }


    public boolean hasModel(String modelId) {
        ModelsDao dao = new ModelsDao();
        ModelsEntity entity = null;

        try {
            entity = dao.findById(modelId);
        } catch (Exception e) {
            JLogger.getLogger(this).error(e.getMessage());
        }

        return entity != null;
    }


    public void persistModel(ClassificationModel model, ModelsEntity descriptor) {
        ModelsDao dao = new ModelsDao();
        String modelPath = this.getModelPath(descriptor.getCorpus().getCorpusId(), descriptor.getModelId());
            try {
                if(new File(modelPath).exists())
                FileUtils.deleteDirectory(new File(modelPath));
            } catch (IOException e) {
                JLogger.getLogger(this).error(e.getMessage());
                e.printStackTrace();
            }
        dao.persist(descriptor);
        model.save(modelPath);
    }

    public ModelsEntity getDescriptor(String modelId) throws NullPointerException {
        ModelsDao dao = new ModelsDao();
        ModelsEntity entity = null;

        try {
            entity = dao.findById(modelId);
        } catch (Exception e) {
            JLogger.getLogger(this).error(e.getMessage());
            return null;
        }
        return entity;
    }

    private String getModelPath(String corpusId, String modelId) {
        String result = null;

        try {
            String modelsDir = PropertiesLoader.load().getProperty("learning.models_dir");
            String path = new StringBuilder().append(modelsDir).append(File.separator).append(corpusId).append(File.separator).toString();
            File file = new File(path);
            if (!file.exists()) {
                file.mkdirs();
            }

            String fullPath = new StringBuilder().append(path).append(modelId).toString();

            result = new File(fullPath).getAbsolutePath();
        } catch (IOException e) {
            JLogger.getLogger(this.getClass().getName()).error("cannot load properties file learning.models_dir");
        }

        return result;
    }

    public Stream<ModelsEntity> getModelStream() {
        ModelsDao dao = new ModelsDao();
        return dao.findAll().stream();
    }

  /*  public void delete_corpus(String corpusId) throws Exception {
        try {
            CorporaEntity corpus = new CorporaDAO().findById(corpusId);
            ModelsDao dao = new ModelsDao();
            if (corpus != null) {
                List<ModelsEntity> models = dao.findByCorpus(corpus);
                for (ModelsEntity model : models)
                    dao.remove(model.getModelId());
            }
        } catch (Exception e) {
            JLogger.getLogger(this).error(e.getMessage());
            e.printStackTrace();
        }
    }*/
}
