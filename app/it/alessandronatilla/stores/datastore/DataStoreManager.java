package it.alessandronatilla.stores.datastore;

import com.google.common.cache.*;
import it.alessandronatilla.indexing.rest.model.WeightScheme;
import it.alessandronatilla.stores.dao.CorporaDAO;
import it.alessandronatilla.stores.model.CorporaEntity;
import it.alessandronatilla.utils.JLogger;
import org.apache.commons.io.FileUtils;
import play.db.jpa.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Classe singleton che permette la gestione centralizzata dell'accesso
 * ai dati di indici lucene.
 *
 * @author Angelo Impedovo
 */
public class DataStoreManager implements Iterable<DataStore> {

    /**
     * Istanza singoletto del gestore
     */
    private static DataStoreManager manager;
    /**
     * Basket per i datastore utilizzati
     */
    private LoadingCache<CorporaEntity, DataStore> _writerCache;


    private DataStoreManager() {
        //cache builder policy
        this._writerCache = CacheBuilder.newBuilder()
                .maximumSize(100)
                .expireAfterAccess(30, TimeUnit.MINUTES)
                .removalListener(new RemovalListener<CorporaEntity, DataStore>() {

                    @Override
                    public void onRemoval(RemovalNotification<CorporaEntity, DataStore> arg0) {
                        DataStore store = arg0.getValue();
                        store.finalize();
                    }

                }).build(new CacheLoader<CorporaEntity, DataStore>() {

                    @Override
                    public DataStore load(CorporaEntity corpus) {
                        WeightScheme scheme = WeightScheme.valueOf(corpus.getWeightName());
                        DataStore store = new DataStore(corpus.getCorpusId(), scheme);
                        return store;
                    }

                });
    }

    /**
     * Metodo statico per prelevare l'istanza singoletto.
     *
     * @return
     */
    public static DataStoreManager getInstance() {
        if (manager == null) {
            manager = new DataStoreManager();
        }
        return manager;
    }

    public static void finalizeInstance() {
        if (manager != null) {
            manager = null;
        }
    }


    @Transactional(readOnly = true)
    public DataStore get(String corpus_id) {
        CorporaDAO dao = new CorporaDAO();
        CorporaEntity corpora = null;
        DataStore store = null;

        try {
            corpora = dao.findById(corpus_id);
            if (corpora != null) {
                store = this._writerCache.get(corpora);
            }
        } catch (ExecutionException e) {
            JLogger.getLogger(this.getClass()).error(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            JLogger.getLogger(this.getClass()).error(e.getMessage());
            e.printStackTrace();
        }

        return store;
    }


    public DataStore create(String corpus_id, WeightScheme schema) throws ExecutionException {
        CorporaDAO dao = new CorporaDAO();
        CorporaEntity corpora = null;
        DataStore store = null;

        try {
            corpora = dao.findById(corpus_id);
            if (corpora == null) {
                corpora = new CorporaEntity(corpus_id, schema.name());
                dao.persist(corpora);
                store = this._writerCache.get(corpora);
            }
        } catch (Exception e) {
            JLogger.getLogger(this.getClass()).error(e.getMessage());
        }


        return store;
    }


    public void remove(String corpus_id) throws Exception {
        CorporaDAO dao = new CorporaDAO();
        CorporaEntity corpora = null;

        try {
            corpora = dao.findById(corpus_id);
            if (corpora == null) {
                throw new IllegalArgumentException("corpus id doesn't exists");
            }
            dao.remove(corpus_id);

            this._writerCache.invalidate(corpora);
            File indexFile = DataStore.getIndexFile(corpus_id);
            FileUtils.deleteDirectory(indexFile);
        } catch (IllegalArgumentException e) {
            JLogger.getLogger(this.getClass()).error(e.getMessage());
            throw e;
        } catch (IOException e) {
            JLogger.getLogger(this.getClass()).error(e.getMessage());
            throw e;
        } catch (Exception e) {
            JLogger.getLogger(this.getClass()).error(e.getMessage());
            throw e;
        }
    }

    @Override
    public Iterator<DataStore> iterator() {
        return this.getActiveIndices().iterator();
    }

    public Collection<DataStore> getActiveIndices() {
        return this._writerCache.asMap().values();
    }

    public Collection<String> getIndicesName() {
        CorporaDAO dao = new CorporaDAO();
        return dao.listAll().stream().map(CorporaEntity::getCorpusId).collect(Collectors.toList());
    }


}
