package it.alessandronatilla.stores.datastore;

import it.alessandronatilla.utils.JLogger;
import it.alessandronatilla.properties.PropertiesLoader;

import java.io.File;
import java.io.IOException;

/**
 * Created by alexander on 4/27/15.
 */
public class FileUtils {

    public static File getMapDBFile() {

        String home = "";
        try {
            home = PropertiesLoader.load().getProperty("index.mapdb_dir");

            if (!new File(home).exists())
                new File(home).mkdirs();

        } catch (IOException e) {
            JLogger.getLogger(FileUtils.class.getName()).error(e.getMessage());
        }
        return new File(home + File.separator + "data");
    }


    public static File getIndexCatalog(String indexID) {

        String home = "";
        try {
            home = PropertiesLoader.load().getProperty("index.data_dir");
            if (!new File(home).exists()) {
                new File(home).mkdir();
            }

            if (!new File(home + File.separator + indexID).exists())
                new File(home + File.separator + indexID).mkdirs();

        } catch (IOException e) {
            JLogger.getLogger(FileUtils.class.getName()).error(e.getMessage());
        }
        return new File(home + File.separator + indexID + File.separator + "data");
    }
}
