package it.alessandronatilla.stores.datastore;

public interface TransactionalDelegate {

    void onExecute(DataStore.Accessor t) throws Exception;

    void onFailure(Exception e);

    void onSuccess();

}