package it.alessandronatilla.stores.datastore;

import it.alessandronatilla.indexing.rest.model.WeightScheme;
import it.alessandronatilla.properties.PropertiesLoader;
import it.alessandronatilla.stores.dao.CorporaDAO;
import it.alessandronatilla.stores.dao.DocumentDAO;
import it.alessandronatilla.stores.model.CorporaEntity;
import it.alessandronatilla.stores.model.DocumentsEntity;
import it.alessandronatilla.utils.JLogger;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

public class DataStore {

    private WeightScheme _scheme;
    private String _id;
    private IndexWriter _luceneWriter;
    private boolean _finalized;

    protected DataStore(String indexId, WeightScheme scheme) {

        this._scheme = scheme;
        this._id = indexId;

        try {
            this._luceneWriter = this.buildWriter(indexId, scheme);
            this._finalized = false;
        } catch (IOException e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
            this._finalized = true;
        }
    }

    static File getIndexFile(String id) {

        String home = "";
        try {
            home = PropertiesLoader.load().getProperty("index.data_dir");

            if (!new File(home).exists())
                new File(home).mkdir();

        } catch (IOException e) {
            JLogger.getLogger(DataStore.class.getName()).error(e.getMessage());
        }
        return new File(home + File.separator + id);
    }

    private IndexWriter buildWriter(String corpus_id, WeightScheme scheme) throws IOException {

        IndexWriter iwriter = null;
        File index_fpath = getIndexFile(corpus_id);

        Directory directory = FSDirectory.open(index_fpath);
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_43, new WhitespaceAnalyzer(Version.LUCENE_43));
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);

        Similarity model = null;
        if (scheme.equals(WeightScheme.TFIDF) || scheme.equals(WeightScheme.TF)) {
            model = new DefaultSimilarity();
        } else if (scheme.equals(WeightScheme.BM25)) {
            model = new BM25Similarity();
        }

        config.setSimilarity(model);
        iwriter = new IndexWriter(directory, config);


        return iwriter;
    }

    public Transaction create(TransactionalDelegate delegate) {
        return new Transaction(delegate);
    }


    public void finalize() {
        this._finalized = true;
        try {
            this._luceneWriter.close();
        } catch (IOException e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
        } catch (java.lang.IllegalAccessError e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
        }
    }


    public WeightScheme getWeightingScheme() {
        return this._scheme;
    }


    public String getIndexId() {
        return this._id;
    }


    /*
     * si deve occupare di gestire integrità referenziale
     * e accesso all'index writer.
     */
    public class Transaction {

        protected boolean _invalidated;
        private TransactionalDelegate _callable;
        private Accessor _dataAccessor;

        private Transaction(TransactionalDelegate delegate) {
            this._callable = delegate;
            this._dataAccessor = new Accessor(this);
            this._invalidated = false;

        }

        private void commit() throws IOException {
            DataStore.this._luceneWriter.commit();
        }

        private void rollback() throws IOException {
            DataStore.this._luceneWriter.rollback();
            DataStore.this._luceneWriter = DataStore.this.buildWriter(_dataAccessor.getIndexID(), _dataAccessor.getWeightScheme());

        }


        public void dispatch() {
            try {
                this.doWork();
            } catch (Exception ex) {
                ex.printStackTrace();
                JLogger.getLogger(this.getClass().getName()).error(ex.getMessage());
            }
        }


        private void doWork() throws Exception {
            try {
                if (!DataStore.this._finalized) {
                    this._callable.onExecute(this._dataAccessor);
                    this.commit();
                    this._callable.onSuccess();
                } else {
                    throw new RuntimeException();
                }
            } catch (Exception ex) {

                try {
                    this.rollback();
                } catch (IOException e) {
                    throw e;
                }

                this._callable.onFailure(ex);
                throw ex;
            }
        }

    }


    public class Accessor {

        private CorporaEntity _corpus;
        private DirectoryReader _reader;
        private Transaction _transaction;

        private Accessor(Transaction transaction) {
            CorporaDAO dao = new CorporaDAO();

            try {
                this._transaction = transaction;
                this._reader = DirectoryReader.open(_luceneWriter, true);
                this._corpus = dao.findById(_id);
            } catch (Exception e) {
                JLogger.getLogger(this.getClass().getName()).info(e.getMessage());
            }
        }

        public WeightScheme getWeightScheme() {
            return getWeightingScheme();
        }

        public Collection<DocumentsEntity> listDocuments() {
            String corpus_id = _corpus.getCorpusId();
            return new DocumentDAO().findByCorpus(corpus_id);
        }

        public void insert(String documentID, Document doc) {
            DocumentDAO dao = new DocumentDAO();
            DocumentsEntity document = new DocumentsEntity(this._corpus, documentID);

            dao.persist(document);

            try {
                DataStore.this._luceneWriter.addDocument(doc);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
//            } else {
//                JLogger.getLogger(this.getClass().getName()).info("throwing exception, corpusID=" + corpusID + ", docID=" + documentID);
//                throw new IllegalArgumentException("documentID " + documentID + " in index " + corpusID + " already exists.");
//            }

        }

        public String getIndexID() {
            return _id;
        }

        public boolean contains(String documentID) {
            DocumentDAO dao = new DocumentDAO();
            DocumentsEntity doc = dao.findByIdAndCorpus(_id, documentID);
            boolean found = doc != null;

            return found;
        }


        public IndexReader getReader() {
            return this._reader;
        }

        public void invalidate() {
            this._transaction._invalidated = true;
        }

        public String getFilePath() {
            return DataStore.getIndexFile(this.getIndexID()).getAbsolutePath();
        }


    }


}
