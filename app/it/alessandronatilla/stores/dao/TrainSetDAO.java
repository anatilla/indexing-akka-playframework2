package it.alessandronatilla.stores.dao;

import it.alessandronatilla.stores.model.DocumentsEntity;
import it.alessandronatilla.stores.model.SplitsEntity;
import it.alessandronatilla.stores.model.TrainsetsEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by alexander on 23/05/15.
 */
public class TrainSetDAO extends AbstractDAO<TrainsetsEntity, Long> {

    public List<DocumentsEntity> getTrainDocuments(SplitsEntity splitId) {
        EntityManager em = this.getEntityManager();

        TypedQuery<DocumentsEntity> query = em.createNamedQuery(
                "TrainSets.getDocuments",
                DocumentsEntity.class).setParameter("splitId", splitId);

        return query.getResultList();

    }

}
