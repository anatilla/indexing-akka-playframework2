package it.alessandronatilla.stores.dao;

import it.alessandronatilla.stores.model.DocumentsEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class DocumentDAO extends AbstractDAO<DocumentsEntity, Long> {

    public DocumentsEntity findByIdAndCorpus(String corpusId, String docId) {
        EntityManager em = this.getEntityManager();

        TypedQuery<DocumentsEntity> query = em.createNamedQuery(
                "DocumentsEntity.findByCorpusAndId",
                DocumentsEntity.class).setParameter("corpusId", corpusId).setParameter("dId", docId);

        List<DocumentsEntity> result = query.getResultList();
        if (!result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public List<DocumentsEntity> findByCorpus(String corpusId) {
        EntityManager em = this.getEntityManager();

        TypedQuery<DocumentsEntity> query = em.createNamedQuery(
                "DocumentsEntity.findByCorpus",
                DocumentsEntity.class).setParameter("corpusId", corpusId);

        List<DocumentsEntity> result = query.getResultList();
        return result;
    }
}
