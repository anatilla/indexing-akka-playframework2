package it.alessandronatilla.stores.dao;

import it.alessandronatilla.stores.model.CorporaEntity;
import it.alessandronatilla.stores.model.ModelsEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by alexander on 22/05/15.
 */
public class ModelsDao extends AbstractDAO<ModelsEntity, String> {

    public List<ModelsEntity> findAll() {
        EntityManager em = this.getEntityManager();

        TypedQuery<ModelsEntity> query = em.createNamedQuery(
                "ModelsEntity.findAll",
                ModelsEntity.class);

        return query.getResultList();
    }

    public List<ModelsEntity> findByCorpus(CorporaEntity corpusId) {
        EntityManager em = this.getEntityManager();

        TypedQuery<ModelsEntity> query = em.createNamedQuery(
                "ModelsEntity.findByCorpus",
                ModelsEntity.class).setParameter("corpus", corpusId);

        List<ModelsEntity> result = query.getResultList();
        return result;
    }
}
