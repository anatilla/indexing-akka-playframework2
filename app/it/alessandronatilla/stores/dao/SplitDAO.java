package it.alessandronatilla.stores.dao;

import it.alessandronatilla.stores.model.SplitsEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by alexander on 23/05/15.
 */
public class SplitDAO extends AbstractDAO<SplitsEntity, String> {

    public List<SplitsEntity> findByCorpus(String corpusId) {
        EntityManager em = this.getEntityManager();

        TypedQuery<SplitsEntity> query = em.createNamedQuery(
                "SplitsEntity.findByCorpus",
                SplitsEntity.class).setParameter("corpusId", corpusId);

        return query.getResultList();
    }
}
