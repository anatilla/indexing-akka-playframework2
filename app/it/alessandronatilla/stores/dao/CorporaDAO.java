package it.alessandronatilla.stores.dao;

import it.alessandronatilla.stores.model.CorporaEntity;
import it.alessandronatilla.stores.modelstore.ModelStore;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class CorporaDAO extends AbstractDAO<CorporaEntity, String> {

	
	public Collection<CorporaEntity> listAll(){
		EntityManager em = this.getEntityManager();
		TypedQuery<CorporaEntity> query = em.createNamedQuery("CorporaEntity.findAll", CorporaEntity.class);
		return query.getResultList();
	}

	@Override
	public void remove(String id) throws Exception {
		ModelStore store = ModelStore.getInstance();
		DocumentDAO documentDAO = new DocumentDAO();
		ModelsDao modelsDao = new ModelsDao();
		TrainSetDAO trainSetDAO = new TrainSetDAO();
		TestSetDAO testSetDAO = new TestSetDAO();
		SplitDAO splitDAO = new SplitDAO();

		


		
		super.remove(id);
	}
}
