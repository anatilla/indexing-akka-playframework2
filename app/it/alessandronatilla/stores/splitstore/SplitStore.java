package it.alessandronatilla.stores.splitstore;

import it.alessandronatilla.analysis.exceptions.SplitNotFoundException;
import it.alessandronatilla.stores.dao.SplitDAO;
import it.alessandronatilla.stores.model.SplitsEntity;
import it.alessandronatilla.utils.JLogger;

import java.util.List;

/**
 * Created by alexander on 16/05/15.
 */

public class SplitStore {

    private static SplitStore instance;

    private SplitStore() {
    }

    public static SplitStore getInstance() {
        if (instance == null) {
            instance = new SplitStore();
        }
        return instance;
    }

    public static void finalizeInstance() {
        if (instance != null) {
            instance = null;
        }
    }

    public SplitsEntity get(String splitId, String corpusId) throws Exception {
        SplitsEntity entity = null;
        try {
            entity = new SplitDAO().findById(splitId);
            if (!entity.getCorpusId().equals(corpusId))
                throw new IllegalArgumentException("no split found");
        } catch (Exception e) {
            JLogger.getLogger(this).error(e.getMessage());
            throw e;
        }
        return entity;
    }

    public void add(SplitsEntity split) {
        new SplitDAO().persist(split);
    }


    public void delete(String corpusId, String splitId) throws Exception {

        if (get(splitId, corpusId) == null)
            throw new SplitNotFoundException("split " + splitId + " referring to " + corpusId + " not found.");
        new SplitDAO().remove(splitId);

    }

    /**
     * delete all split referred to a given corpus
     *
     * @param corpusId id of the given corpus
     */
    public void delete(String corpusId) {
        SplitDAO dao = new SplitDAO();
        List<SplitsEntity> results = dao.findByCorpus(corpusId);

        results.forEach(splitsEntity -> {
            try {
                dao.remove(splitsEntity.getSplitId());
            } catch (Exception e) {
                JLogger.getLogger(this).error(e.getMessage());
            }
        });
    }

    public List<SplitsEntity> getAll(String corpusId) {
        return new SplitDAO().findByCorpus(corpusId);
    }
}
