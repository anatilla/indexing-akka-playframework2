package it.alessandronatilla.indexing.actors.session;

import akka.actor.ActorRef;

/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class SessionStatus {
    private final Integer dataSize;
    private final ActorRef sender;
    private String uuid;
    private Integer counter;

    public SessionStatus(String uuid, Integer dataSize, ActorRef sender) {
        this.uuid = uuid;
        this.dataSize = dataSize;
        this.counter = 0;
        this.sender = sender;
    }

    public String getUuid() {
        return uuid;
    }

    public Integer getCounter() {
        return counter;
    }

    public void incrementCounter() {
        this.counter++;
    }

    public Integer getDataSize() {
        return dataSize;
    }

    public ActorRef getSender() {
        return sender;
    }

    @Override
    public String toString() {
        return "SessionStatus{" + "counter=" + counter + ", uuid='" + uuid + '\'' + ", dataSize=" + dataSize + '}';
    }
}
