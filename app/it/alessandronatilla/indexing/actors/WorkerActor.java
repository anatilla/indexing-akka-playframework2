package it.alessandronatilla.indexing.actors;

import akka.actor.UntypedActor;
import it.alessandronatilla.indexing.messages.ResultMessage;
import it.alessandronatilla.indexing.messages.Work;
import it.alessandronatilla.indexing.messages.WorkMessage;
import it.alessandronatilla.indexing.pipelines.steps.nlp.exceptions.KitemurtException;
import it.alessandronatilla.utils.JLogger;
import play.db.jpa.JPA;

/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class WorkerActor extends UntypedActor {

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof WorkMessage) {
            WorkMessage work = (WorkMessage) o;
            Work unitOfWork = work.getUnitOfWork();
            String id = unitOfWork.getId();

            JLogger.getLogger(this.getClass().getName()).info("Working on Pipeline #" + id + ": " + work.getUuid());
            JLogger.getLogger(this.getClass().getName()).info("Received " + work.getClass().getName() + " from " + getSender());
            Object result = null;

            try {
                result = JPA.withTransaction(() -> unitOfWork.doWork());

            } catch (Throwable throwable) {
                throw new KitemurtException(throwable.getMessage(), work.getUuid(), id);
            }


            JLogger.getLogger(this.getClass().getName()).info("Launched Pipeline #" + id + ": " + work.getUuid());

            ResultMessage res = new ResultMessage(result, work.getUuid(), unitOfWork.getId(), true);
            JLogger.getLogger(this.getClass().getName()).info("Sending " + res.getClass().getName() + " to " + getSender());

            getSender().tell(res, getSelf());

        } else unhandled(o);
    }
}
