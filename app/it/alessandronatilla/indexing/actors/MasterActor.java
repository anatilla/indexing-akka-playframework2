package it.alessandronatilla.indexing.actors;

import akka.actor.*;
import akka.routing.RoundRobinPool;
import it.alessandronatilla.indexing.actors.session.SessionStatus;
import it.alessandronatilla.indexing.messages.InitMessage;
import it.alessandronatilla.indexing.messages.ResultMessage;
import it.alessandronatilla.indexing.messages.Work;
import it.alessandronatilla.indexing.messages.WorkMessage;
import it.alessandronatilla.indexing.pipelines.steps.nlp.exceptions.KitemurtException;
import it.alessandronatilla.indexing.rest.model.Document;
import it.alessandronatilla.indexing.rest.model.FinalResult;
import it.alessandronatilla.utils.JLogger;
import scala.concurrent.duration.Duration;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static akka.actor.SupervisorStrategy.escalate;
import static akka.actor.SupervisorStrategy.restart;


/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class MasterActor extends UntypedActor {
    private ActorRef workerRouter;
    private ActorRef self;
    private FinalResult global_result;
    private Map<String, SessionStatus> sessionStatusMap;
    private SupervisorStrategy strategy;


    public MasterActor(final int num_actors) {

        this.sessionStatusMap = new HashMap<>();
        this.self = getSelf();
        this.strategy = new OneForOneStrategy(-1, Duration.Inf(),
                t -> {
                    if (t instanceof KitemurtException) {
                        KitemurtException ex = (KitemurtException) t;

                        ResultMessage message = new ResultMessage(ex.getMessage(), ex.getSessionId(), ex.getDocId(), false);
                        self.tell(message, getSelf());
                        return restart();
                    } else {
                        return escalate();
                    }

                }
        );

        this.workerRouter = getContext().actorOf(
                Props.create(WorkerActor.class).withRouter(
                        new RoundRobinPool(num_actors).withSupervisorStrategy(this.strategy)
                ), "workerRouter");
    }

    @Override
    public void onReceive(Object o) throws Exception {

        if (o instanceof InitMessage<?, ?>) {
            global_result = new FinalResult();
            InitMessage<?, ?> calc = (InitMessage<?, ?>) o;
            Collection<?> dataset = calc.getDataset();
            String uuid = calc.getUuid();

            sessionStatusMap.put(uuid, new SessionStatus(uuid, dataset.size(), getSender()));
            JLogger.getLogger(this.getClass().getName()).info("Received " + calc.getClass().getName() + " from " + getSender());

            Stream<Work> streamOfWorks = calc.getStream();
            streamOfWorks.forEach(work -> {
                WorkMessage msg = new WorkMessage(work, uuid);
                JLogger.getLogger(this.getClass().getName()).info("Sending " + msg.getClass().getName() + " to " + workerRouter);
                workerRouter.tell(msg, getSelf());
            });

        } else if (o instanceof ResultMessage) {

            ResultMessage res = (ResultMessage) o;
            SessionStatus sessionStatus = sessionStatusMap.get(res.getUuid());
            JLogger.getLogger(this.getClass().getName()).info("Received " + res.getClass().getName() + " from " + getSender());

            Document document = new Document(res.getWork_id(), res.getResult());
            if (res.isSucceeded())
                global_result.addSucceeded(document);
            else global_result.addUnsucceeded(document);

            sessionStatus.incrementCounter();
            sessionStatusMap.put(sessionStatus.getUuid(), sessionStatus);
            JLogger.getLogger(this.getClass().getName()).info("Counter = " + sessionStatus.getCounter() + ", totalData = " + sessionStatus.getDataSize());
            JLogger.getLogger(this.getClass().getName()).info("Request status Map size: " + sessionStatusMap.size());

            if (sessionStatus.getCounter().compareTo(sessionStatus.getDataSize()) == 0) {
                ActorRef sender = sessionStatus.getSender();
                JLogger.getLogger(this.getClass().getName()).info("Sending final result " + global_result.getClass().getName() + " of size:" + global_result.size() + " to " + sender);
                sender.tell(global_result, getSelf());
            }
        } else
            unhandled(o);
    }
}
