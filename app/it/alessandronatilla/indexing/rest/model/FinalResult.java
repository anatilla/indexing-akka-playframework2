package it.alessandronatilla.indexing.rest.model;

import play.libs.F;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by alexander on 4/29/15.
 */
public class FinalResult implements F.Function0<Map> {
    private List<Document> succeededResults;
    private List<Document> unsucceededResults;

    public FinalResult() {
        this.succeededResults = new LinkedList<>();
        this.unsucceededResults = new LinkedList<>();
    }

    public void addSucceeded(Document document) {
        this.succeededResults.add(document);
    }

    public void addUnsucceeded(Document document) {
        this.unsucceededResults.add(document);
    }

    public List<Document> getSucceded() {
        return this.succeededResults;
    }

    public List<Document> getUnsucceeded() {
        return this.unsucceededResults;
    }

    public Integer size() {
        return this.succeededResults.size() + this.unsucceededResults.size();
    }

    @Override
    public String toString() {
        List<String> successful = succeededResults.stream().map(d -> d.getId()).collect(Collectors.toList());
        List<UnsuccessfulReport> unsuccessful = unsucceededResults.stream().map(d -> new UnsuccessfulReport(d.getId(), d.getBody().toString()))
                .collect(Collectors.toList());

        return "FinalResult{" +
                "succeededResults=" + successful +
                ", unsucceededResults=" + unsuccessful +
                '}';
    }

    @Override
    public Map apply() throws Throwable {
        List<String> successful = succeededResults.stream().map(d -> d.getId()).collect(Collectors.toList());
        List<Document> unsuccessful = unsucceededResults.stream().collect(Collectors.toList());

        Map<String, Object> result = new HashMap<>();
        result.put("ok", successful);
        result.put("error", unsuccessful);
        return result;
    }

    public class UnsuccessfulReport {
        private String id;
        private String description;

        public UnsuccessfulReport(String id, String description) {
            this.id = id;
            this.description = description;
        }

        @Override
        public String toString() {
            return "UnsuccessfulReport{" +
                    "id=" + id +
                    ", description='" + description + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof UnsuccessfulReport)) return false;
            UnsuccessfulReport that = (UnsuccessfulReport) o;
            return Objects.equals(id, that.id);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }
}
