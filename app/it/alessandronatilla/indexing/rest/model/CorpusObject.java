package it.alessandronatilla.indexing.rest.model;

import java.util.List;

/**
 * Author: alexander
 * Project: akka-es-indexing-service
 */
public class CorpusObject {
    private List<TextContent> content;
    private String lang;
    private String id;


    public CorpusObject(String lang, List<TextContent> content, String id) {
        this.id = id;
        this.lang = lang;
        this.content = content;
    }

    public CorpusObject() {
    }

    public String getLang() {
        return lang;
    }

    public List<TextContent> getContent() {
        return content;
    }

    public String getId() {
        return id;
    }

}
