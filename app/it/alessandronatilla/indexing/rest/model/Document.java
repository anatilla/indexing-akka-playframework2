package it.alessandronatilla.indexing.rest.model;

/**
 * Author: alexander
 * Project: akka-es-indexing-service
 */
public class Document {
    private final Object body;
    private final String id;

    public Document(String id, Object body) {
        this.id = id;
        this.body = body;
    }

    public String getId() {
        return id;
    }

    public Object getBody() {
        return body;
    }

    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", body=" + body +
                '}';
    }
}
