package it.alessandronatilla.indexing.rest.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by alexander on 4/13/15.
 */

@XmlRootElement
public interface UniqueContent<V> {
    Comparable getId();

    V getContent();

}
