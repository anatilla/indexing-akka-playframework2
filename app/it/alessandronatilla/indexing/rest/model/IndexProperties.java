package it.alessandronatilla.indexing.rest.model;

/**
 * Author: alexander
 * Project: akka-es-indexing-service
 */
public class IndexProperties {

    private String id;
    private WeightScheme schema;

    public IndexProperties(String id, WeightScheme schema) {
        this.id = id;
        this.schema = schema;
    }

    public IndexProperties() {
    }

    public String getId() {
        return id;
    }

    public WeightScheme getSchema() {
        return schema;
    }
}
