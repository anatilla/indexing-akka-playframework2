package it.alessandronatilla.indexing.rest.model;

import java.util.Map;

/**
 * Author: alexander
 * Project: akka-es-indexing-service
 */
public class TextContent {
    private String id;
    private String content;
    private Map<String, Object> targets;  //attributo => valore

    public TextContent(String id, String content, Map<String, Object> targets) {
        this.content = content;
        this.id = id;
        this.targets = targets;
    }

    public TextContent() {

    }

    public String getContent() {
        return content;
    }

    public String getId() {
        return id;
    }

    public Map<String, Object> getTargets() {
        return targets;
    }
}
