package it.alessandronatilla.indexing.rest.services;

import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.stores.datastore.DataStoreManager;
import it.alessandronatilla.indexing.rest.model.IndexProperties;
import it.alessandronatilla.indexing.rest.model.WeightScheme;
import it.alessandronatilla.utils.JLogger;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Author: alexander
 * Project: akka-es-indexing-service
 */
public class IndexCreationService {

    public static Map getData(
            IndexProperties indexProperties) {

        Map<String, String> response = new HashMap<>();

        if (indexProperties == null ||
                indexProperties.getId() == null ||
                indexProperties.getSchema() == null) {
            JLogger.getLogger(IndexCreationService.class.getName()).info("Invalid request: empty request received");
            response.put("error", "empty request. please ty again.");
            return response;
        }

        WeightScheme scheme = indexProperties.getSchema();
        String corpus_id = indexProperties.getId();
        DataStoreManager manager = DataStoreManager.getInstance();

        DataStore store = manager.get(corpus_id);
        if (store == null) {
            try {
                manager.create(corpus_id, scheme);
                JLogger.getLogger(IndexCreationService.class.getName()).info("Index " + corpus_id + " created");
                response.put("OK", "Index " + corpus_id + " successfully created.");
                return response;
            } catch (ExecutionException e) {
                JLogger.getLogger(IndexCreationService.class.getName()).info("ExecutionException during creation");
                response.put("Error", "Something went wrong.");
                return response;
            }
        } else {
            JLogger.getLogger(IndexCreationService.class.getName()).info("Invalid index id");
            response.put("Error", "Index " + corpus_id + " already exists.");
            return response;
        }
    }

}
