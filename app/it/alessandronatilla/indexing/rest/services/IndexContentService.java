package it.alessandronatilla.indexing.rest.services;

import akka.pattern.Patterns;
import akka.util.Timeout;
import it.alessandronatilla.indexing.messages.InitMessage;
import it.alessandronatilla.indexing.pipelines.Pipeline;
import it.alessandronatilla.indexing.pipelines.steps.nlp.*;
import it.alessandronatilla.indexing.pipelines.steps.nlp.model.TextUnit;
import it.alessandronatilla.indexing.rest.model.CorpusObject;
import it.alessandronatilla.indexing.rest.model.FinalResult;
import it.alessandronatilla.indexing.rest.model.TextContent;
import it.alessandronatilla.preprocessing.model.Language;
import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.stores.datastore.DataStoreManager;
import it.alessandronatilla.stores.datastore.TransactionalDelegate;
import it.alessandronatilla.utils.JLogger;
import it.alessandronatilla.utils.TimeProvider;
import play.db.jpa.Transactional;
import play.libs.Akka;
import play.libs.F;
import play.libs.F.Promise;
import scala.concurrent.duration.Duration;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class IndexContentService {

    private HashMap<String, Object> global_response = new HashMap<String, Object>();
    private Map final_result = null;
    private DataStore.Accessor t;

    public Pipeline<TextContent, TextUnit> createPreprocessingPipelinePrototype(DataStore.Accessor accessor, Language language, String indexId) {
        return Pipeline.createPipeline(new ContentExtractor(accessor))
                .addStep(new Segmenter(language))
                .addStep(new Tokenizer(language))
                .addStep(new PunctuationRemover())
                .addStep(new StopwordRemover(language))
                .addStep(new PartOfSpeechTagger(language))
                .addStep(new Stemmer(language))
                .addStep(new TextTransformer(indexId, accessor));
    }

    @Transactional
    public Object getData(CorpusObject object, String uuid) {

        //questo metodo dovrebbe indicizzare i contenuti su lucene mantenendo
        //consistente, l'indice primario.
        DataStoreManager manager = DataStoreManager.getInstance();
        DataStore dataStore;

        dataStore = manager.get(object.getId());
        if (dataStore == null) {
            JLogger.getLogger(this.getClass().getName()).info("Invalid request: cannot find any index having ID:" + object.getId());
            global_response.put("error", "any index with id " + object.getId() + " exists. please build it using /rest/create API");

            return global_response;
        }

        if (object.getContent().isEmpty()) {
            JLogger.getLogger(this.getClass().getName()).info("Invalid request: empty corpus");
            global_response.put("error", "empty corpus submitted");
            return global_response;
        }

        if (object.getLang().isEmpty()) {
            JLogger.getLogger(this.getClass().getName()).info("Invalid request: empty language");
            global_response.put("error", "no language string submitted.");
            return global_response;
        }

        TimeProvider timeProvider = TimeProvider.getInstance();
        String session = object.getId() + ":" + object.getLang() + ":" + UUID.randomUUID().toString();
        timeProvider.create(session);
        timeProvider.start(session);

        IndexingDelegate delegate = new IndexingDelegate(object, uuid);
        dataStore.create(delegate).dispatch();

        Long elapsed = timeProvider.getElapsed(session);
        JLogger.getLogger(this).info("Elapsed indexing time for " + session + ": " + elapsed + " ms");
        global_response.put("elapsed", elapsed);

        return delegate.getResult();
    }


    private class IndexingDelegate implements TransactionalDelegate {

        private CorpusObject _corpus;
        private FinalResult _result;
        private String uuid;
        private Promise<Object> promise;

        private IndexingDelegate(CorpusObject corpus, String uuid) {
            this._corpus = corpus;
            this.uuid = uuid;
        }

        @Override
        public void onExecute(DataStore.Accessor dataAccessor) {

            Language language;
            if (this._corpus.getLang().toLowerCase().equals("it")) {
                language = Language.IT;
            } else {
                language = Language.EN;
            }

            String sessionKey = uuid + ":" + this._corpus.getId();

            Pipeline<TextContent, TextUnit> pipeline = IndexContentService.this.createPreprocessingPipelinePrototype(
                    dataAccessor, language, this._corpus.getId()
            );

            InitMessage<TextContent, TextUnit> calc = new InitMessage<TextContent, TextUnit>(pipeline, this._corpus.getContent(), sessionKey);
            Timeout timeout = new Timeout(Duration.create(300, TimeUnit.HOURS));

            JLogger.getLogger(this.getClass().getName()).info("Received indexing content request for corpus " + this._corpus.getId() + ". " +
                    "Indexing " + this._corpus.getContent().size() + " contents for language " + this._corpus.getLang() + ". Request UUID=" + sessionKey);

            promise = F.Promise.wrap(Patterns.ask(Akka.system().actorSelection("/user/" + uuid), calc, timeout));

        }

        @Override
        public void onFailure(Exception e) {
            e.printStackTrace();
            JLogger.getLogger(this).error(e.getLocalizedMessage());

        }

        @Override
        public void onSuccess() {
            this._result = (FinalResult) promise.get(300, TimeUnit.HOURS);
        }

        public FinalResult getResult() {
            return this._result;
        }

    }

    private class ErrorResult implements F.Function0<Map> {
        Map<String, Object> error_result;

        public ErrorResult(Map<String, Object> result) {
            this.error_result = result;
        }

        @Override
        public Map apply() throws Throwable {
            return error_result;
        }
    }
}
