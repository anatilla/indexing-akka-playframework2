package it.alessandronatilla.indexing.rest.services;


import it.alessandronatilla.stores.datastore.DataStoreManager;
import it.alessandronatilla.utils.JLogger;

import java.util.HashMap;
import java.util.Map;

public class IndexDeletionService {

    public static Map<String, String> getData(String indexId) {
        Map<String, String> result = new HashMap<String, String>();
        DataStoreManager manager = DataStoreManager.getInstance();

        try {
            manager.remove(indexId);
            result.put("Ok", "Index " + indexId + " deleted.");
            JLogger.getLogger(IndexDeletionService.class.getName()).info("Deleted index:" + indexId + ".");
        } catch (IllegalArgumentException ex) {
            result.put("Error", "Index " + indexId + " doesn't exists.");
            return result;
        } catch (Exception ex) {
            result.put("Error", "Something went wrong for index " + indexId + ".");
            return result;
        }

        return result;
    }

}
