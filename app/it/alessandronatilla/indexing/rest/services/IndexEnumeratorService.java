package it.alessandronatilla.indexing.rest.services;

import it.alessandronatilla.stores.datastore.DataStoreManager;
import it.alessandronatilla.utils.JLogger;

import java.util.LinkedList;
import java.util.List;

/**
 * Author: alexander
 * Project: akka-es-indexing-service
 */


public class IndexEnumeratorService {

    public static List<String> getData() {

        DataStoreManager manager = DataStoreManager.getInstance();
        List<String> availableIndexes = new LinkedList<>();

        for (String index : manager.getIndicesName()) {
            availableIndexes.add(index);
        }

        JLogger.getLogger(IndexEnumeratorService.class.getName()).info("Requesting enumeration of available indices.");
        return availableIndexes;
    }
}
