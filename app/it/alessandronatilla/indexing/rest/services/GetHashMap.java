package it.alessandronatilla.indexing.rest.services;

import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.stores.datastore.DataStoreManager;
import it.alessandronatilla.stores.datastore.TransactionalDelegate;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Author: alexander
 * Project: akka-es-indexing-service
 */
public class GetHashMap {

    public static Map print() {
        Map<String, Object> indexKeys = new TreeMap<>();
        DataStoreManager manager = DataStoreManager.getInstance();

        for (String index : manager.getIndicesName()) {
            DataStore dataStore = null;

            dataStore = manager.get(index);
            dataStore.create(new TransactionalDelegate() {
                @Override
                public void onExecute(DataStore.Accessor t) {
                    HashMap<String, Object> obj = new HashMap<String, Object>();

                    List<String> indices = t.listDocuments().stream()
                            .map(s -> s.getDocumentId())
                            .collect(Collectors.toList());

                    Long count = indices.stream().count();
                    obj.put("count", count);
                    indexKeys.put(index, obj);

                }

                @Override
                public void onFailure(Exception e) {
                    indexKeys.put("status", "error");
                    indexKeys.put("detail", e.toString());

                }

                @Override
                public void onSuccess() {

                }
            }).dispatch();


        }
        return indexKeys;
    }
}
