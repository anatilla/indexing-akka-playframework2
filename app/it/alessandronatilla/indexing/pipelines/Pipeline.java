package it.alessandronatilla.indexing.pipelines;

import java.util.function.Function;

/**
 * @param <I> Class specifying input type
 * @param <O> Class specifying output type
 * @author Angelo Impedovo
 *         Defines a customizable pipeline of operations on input of type I
 *         which will produce output of type O.
 */
public class Pipeline<I, O> implements Function<I, O> {

    private static Integer id = 0;
    private Step<I, ?> rootStep;
    private Step<?, O> lastStep;


    /**
     * Friendly constructor of a pipeline with an initial step.
     *
     * @param rootStep Initial step for the pipeline to be created by the constructor.
     */
    Pipeline(Step<I, O> rootStep) {
        this.rootStep = rootStep;
        this.lastStep = rootStep;
    }

    /**
     * Generic static method to be used as a factory-of-pipelines instantiation
     * procedure.
     * Masks details over initial input/output generic arguments to the developer.
     *
     * @param functional Initial functional to be used inside the
     *                   first step for the pipeline to be created by the method.
     * @return
     */
    public static <T1, T2> Pipeline<T1, T2> createPipeline(Function<T1, T2> functional) {
        Step<T1, T2> firstStep = new Step<T1, T2>(functional);
        return new Pipeline<T1, T2>(firstStep);
    }

    public Integer getId() {
        return id;
    }

    /**
     * Checks if the pipeline has either an initial step and a last step.
     *
     * @return true if the pipeline is empty, false otherwise
     */
    public boolean isEmpty() {
        return (this.rootStep == null || this.lastStep == null);
    }


    /**
     * Generic method which extends the pipeline with an extra step.
     * Returns a new pipeline over the old pipeline input type
     * and the extra step output type.
     *
     * @param nextFunctional
     * @return
     */
    public <T> Pipeline<I, T> addStep(Function<O, T> nextFunctional) {
        //mantenere la lista dei task
        Step<?, O> last = this.lastStep;
        Step<O, T> nextStep = new Step<O, T>(nextFunctional);
        last.nextStep = nextStep;

        Pipeline<I, T> pipeline = new Pipeline<I, T>(null);
        pipeline.rootStep = this.rootStep;
        pipeline.lastStep = nextStep;
        return pipeline;
    }


    /**
     * Runs the entire pipeline over a specified input.
     *
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public O apply(I arg) {
        Step currentStep = this.rootStep;
        Step lastExecutedStep = null;
        Object currentInput = arg;
        Object currentOutput = null;
        O finalOutput = null;
        id++;

        while (currentStep != null && lastExecutedStep != this.lastStep) {
            currentOutput = currentStep.apply(currentInput);
            currentInput = currentOutput;

            lastExecutedStep = currentStep;
            currentStep = currentStep.nextStep;
        }

        if (currentOutput != null) {
            finalOutput = (O) currentOutput;
        }

        return finalOutput;
    }


}
