package it.alessandronatilla.indexing.pipelines.exceptions;

/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class EmptyPipelineException extends RuntimeException {
    public EmptyPipelineException() {
        super();
    }

    public EmptyPipelineException(String message) {
        super(message);
    }

    public EmptyPipelineException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmptyPipelineException(Throwable cause) {
        super(cause);
    }

    protected EmptyPipelineException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
