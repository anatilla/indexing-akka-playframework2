package it.alessandronatilla.indexing.pipelines.steps.nlp;

import it.alessandronatilla.analysis.model.matrix.IndexedField;
import it.alessandronatilla.indexing.pipelines.steps.nlp.model.StemmedContent;
import it.alessandronatilla.indexing.pipelines.steps.nlp.model.TextUnit;
import it.alessandronatilla.preprocessing.model.StemmedWord;
import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.utils.JLogger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;

import java.util.Map;
import java.util.function.Function;

public class TextTransformer implements Function<StemmedContent, TextUnit> {

    private String indexId;
    private FieldType textFieldNotStoredWithPosition;
    private FieldType textFieldStoredWithPosition;
    private DataStore.Accessor accessor;

    public TextTransformer(String indexId, DataStore.Accessor accessor) {
        this.indexId = indexId;
        this.accessor = accessor;

        this.textFieldNotStoredWithPosition = new FieldType(StringField.TYPE_NOT_STORED);
        this.textFieldNotStoredWithPosition.setTokenized(true);
        this.textFieldNotStoredWithPosition.setStoreTermVectors(true);
        this.textFieldNotStoredWithPosition.setStoreTermVectorPositions(true);

        this.textFieldStoredWithPosition = new FieldType(StringField.TYPE_STORED);
        this.textFieldStoredWithPosition.setTokenized(true);
        this.textFieldStoredWithPosition.setStoreTermVectors(true);
        this.textFieldStoredWithPosition.setStoreTermVectorPositions(true);
    }


    @Override
    public TextUnit apply(StemmedContent arg0) {
        StringBuilder stemmedBuilder = new StringBuilder();
        StringBuilder lemmatizedBuilder = new StringBuilder();
        StringBuilder postagTokenBuilder = new StringBuilder();
        StringBuilder tokenBuilder = new StringBuilder();

        for (StemmedWord word : arg0.getContent()) {
            stemmedBuilder.append(word.getStem()).append(" ");
            lemmatizedBuilder.append(word.getLemma()).append(" ");
            postagTokenBuilder.append(word.getToken()).append(":").append(word.getPosTag()).append(" ");
            tokenBuilder.append(word.getToken()).append(" ");
        }

        Document document = new Document();
        document.add(new StringField("id", arg0.getId().toString(), Field.Store.YES));
        document.add(new Field(IndexedField.lemmas.name(), lemmatizedBuilder.toString(), this.textFieldStoredWithPosition));
        document.add(new Field(IndexedField.stems.name(), stemmedBuilder.toString(), this.textFieldStoredWithPosition));
        document.add(new Field(IndexedField.postagtoken.name(), postagTokenBuilder.toString(), this.textFieldStoredWithPosition));
        document.add(new Field(IndexedField.tokens.name(), tokenBuilder.toString(), this.textFieldStoredWithPosition));

        // add target attr:values to index
        Map<String, Object> targetAttr = arg0.getTargets();
        targetAttr.forEach((k, v) -> document.add(new StringField(k, v.toString(), Field.Store.YES)));

        TextUnit unit = new TextUnit(lemmatizedBuilder.toString(), stemmedBuilder.toString(), targetAttr);

        Long now = System.currentTimeMillis();
        try {
            accessor.insert(arg0.getId().toString(), document);
        } catch (javax.persistence.PersistenceException e) {
            throw new IllegalArgumentException("Document " + arg0.getId() + " already exists in index " + this.indexId);
        }
        JLogger.getLogger(this).info("Lucene indexing elapsed: " + (System.currentTimeMillis() - now) + " ms");
        JLogger.getLogger(this.getClass().getName()).info("indexed at: " + this.indexId + ", document: " + arg0.getId());

        return unit;
    }

}
