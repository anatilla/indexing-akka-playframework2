package it.alessandronatilla.indexing.pipelines.steps.nlp.exceptions;

/**
 * Created by alexander on 4/28/15.
 */
public class KitemurtException extends RuntimeException {

    private String _sessionId;
    private String _docId;
    private String _message;


    public KitemurtException(String s, String sessionId, String docId) {
        this._sessionId = sessionId;
        this._message = s;
        this._docId = docId;
    }

    public String getSessionId() {
        return this._sessionId;
    }

    public String getDocId() {
        return _docId;
    }

    public String getMessage() {
        return _message;
    }
}
