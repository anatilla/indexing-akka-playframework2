package it.alessandronatilla.indexing.pipelines.steps.nlp.model;

import java.util.Map;

public class TextUnit {

    private final Map<String, Object> targets;  //attributo => valore
    private String lemmas;
    private String stems;


    public TextUnit(String lemmas, String stems, Map<String, Object> targets) {
        this.targets = targets;
        this.lemmas = lemmas;
        this.stems = stems;
    }


    public String getLemmas() {
        return lemmas;
    }


    public String getStems() {
        return stems;
    }

    public Map<String, Object> getTargets() {
        return targets;
    }

    @Override
    public String toString() {
        return "TextUnit{" +
                "lemmas='" + lemmas + '\'' +
                ", stems='" + stems + '\'' +
                ", targets=" + targets +
                '}';
    }
}
