package it.alessandronatilla.indexing.pipelines.steps.nlp.model;

import java.util.List;
import java.util.Map;

/**
 * Author: alexander
 * Project: akka-es-indexing-service
 */
public class NopunctuatedContent {
    private final String id;
    private final List<String> content;
    private final Map<String, Object> targets;  //attributo => valore


    public NopunctuatedContent(List<String> content, String id, Map<String, Object> target) {
        this.content = content;
        this.id = id;
        this.targets = target;
    }

    public List<String> getContent() {
        return content;
    }

    public Map<String, Object> getTargets() {
        return targets;
    }

    public String getId() {
        return id;
    }
}
