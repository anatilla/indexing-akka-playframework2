package it.alessandronatilla.indexing.pipelines.steps.nlp.model;

import it.alessandronatilla.preprocessing.model.TaggedWord;

import java.util.List;
import java.util.Map;

/**
 * Author: alexander
 * Project: akka-es-indexing-service
 */
public class TaggedContent {
    private final String id;
    private final List<TaggedWord> content;
    private final Map<String, Object> targets;  //attributo => valore


    public TaggedContent(List<TaggedWord> content, String id, Map<String, Object> targets) {
        this.content = content;
        this.id = id;
        this.targets = targets;
    }



    public List<TaggedWord> getContent() {
        return content;
    }

    public Map<String, Object> getTargets() {
        return targets;
    }

    public String getId() {
        return id;
    }
}
