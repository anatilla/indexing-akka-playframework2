package it.alessandronatilla.indexing.pipelines.steps.nlp.model;

import java.util.List;
import java.util.Map;

/**
 * Author: alexander
 * Project: akka-es-indexing-service
 */
public class SegmentedContent {
    private final String id;
    private final List<String> content;
    private final Map<String, Object> targets;  //attributo => valore


    public SegmentedContent(List<String> content, String id, Map<String, Object> targets) {
        this.content = content;
        this.id = id;
        this.targets = targets;
    }

    public Map<String, Object> getTargets() {
        return targets;
    }

    public List<String> getContent() {
        return content;
    }

    public String getId() {
        return id;
    }
}
