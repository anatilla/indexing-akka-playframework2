package it.alessandronatilla.indexing.pipelines.steps.nlp.model;

import it.alessandronatilla.preprocessing.model.StemmedWord;

import java.util.List;
import java.util.Map;

/**
 * Author: alexander
 * Project: akka-es-indexing-service
 */
public class StemmedContent {
    private final String id;
    private final List<StemmedWord> stems;
    private final Map<String, Object> targets;  //attributo => valore



    public StemmedContent(String id, List<StemmedWord> stems, Map<String, Object> targets) {
        this.id = id;
        this.stems = stems;
        this.targets = targets;
    }

    public String getId() {
        return id;
    }

    public Map<String, Object> getTargets() {
        return targets;
    }

    public List<StemmedWord> getContent() {
        return stems;
    }

    public List<StemmedWord> getStems() {
        return stems;
    }
}
