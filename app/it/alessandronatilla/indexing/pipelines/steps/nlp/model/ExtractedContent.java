package it.alessandronatilla.indexing.pipelines.steps.nlp.model;

import java.util.Map;

/**
 * Author: alexander
 * Project: akka-es-indexing-service
 */
public class ExtractedContent {
    private final String id;
    private final String content;
    private final Map<String, Object> targets;  //attributo => valore


    public ExtractedContent(String content, String id, Map<String, Object> targets) {
        this.content = content;
        this.id = id;
        this.targets = targets;
    }

    public String getContent() {
        return content;
    }

    public String getId() {
        return id;
    }

    public Map<String, Object> getTargets() {
        return targets;
    }
}
