package it.alessandronatilla.indexing.pipelines.steps.nlp;

import it.alessandronatilla.indexing.pipelines.steps.nlp.model.StemmedContent;
import it.alessandronatilla.indexing.pipelines.steps.nlp.model.TaggedContent;
import it.alessandronatilla.preprocessing.TextPreProcessor;
import it.alessandronatilla.preprocessing.model.Language;
import it.alessandronatilla.preprocessing.model.StemmedWord;
import it.alessandronatilla.utils.JLogger;

import java.util.List;
import java.util.function.Function;

/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class Stemmer implements Function<TaggedContent, StemmedContent> {

    protected Language language;

    public Stemmer(Language language) {
        this.language = language;
    }

    @Override
    public StemmedContent apply(TaggedContent objects) {

        Long now = System.currentTimeMillis();
        List<StemmedWord> stems = TextPreProcessor.stem(language, objects.getContent());
        JLogger.getLogger(this).info("Stemming elapsed: " + (System.currentTimeMillis() - now) + " ms");

        return new StemmedContent(objects.getId(), stems, objects.getTargets());
    }
}
