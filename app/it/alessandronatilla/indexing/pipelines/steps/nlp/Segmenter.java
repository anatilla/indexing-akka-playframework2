package it.alessandronatilla.indexing.pipelines.steps.nlp;

import it.alessandronatilla.indexing.pipelines.steps.nlp.model.ExtractedContent;
import it.alessandronatilla.indexing.pipelines.steps.nlp.model.SegmentedContent;
import it.alessandronatilla.preprocessing.TextPreProcessor;
import it.alessandronatilla.preprocessing.model.Language;
import it.alessandronatilla.utils.JLogger;

import java.util.List;
import java.util.function.Function;

/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class Segmenter implements Function<ExtractedContent, SegmentedContent> {

    protected Language language;

    public Segmenter(Language language) {
        this.language = language;
    }

    @Override
    public SegmentedContent apply(ExtractedContent strings) {

        Long now = System.currentTimeMillis();
        List<String> segment = TextPreProcessor.segment(language, strings.getContent());
        JLogger.getLogger(this.getClass().getName()).info("Segmentation elapsed: " + (System.currentTimeMillis() - now) + " ms");

        return new SegmentedContent(segment, strings.getId(), strings.getTargets());
    }
}
