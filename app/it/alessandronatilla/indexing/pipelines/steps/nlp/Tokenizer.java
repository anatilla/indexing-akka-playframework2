package it.alessandronatilla.indexing.pipelines.steps.nlp;

import it.alessandronatilla.indexing.pipelines.steps.nlp.model.SegmentedContent;
import it.alessandronatilla.indexing.pipelines.steps.nlp.model.TokenizedContent;
import it.alessandronatilla.preprocessing.TextPreProcessor;
import it.alessandronatilla.preprocessing.model.Language;
import it.alessandronatilla.utils.JLogger;

import java.util.List;
import java.util.function.Function;

/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class Tokenizer implements Function<SegmentedContent, TokenizedContent> {

    protected Language language;

    public Tokenizer(Language language) {
        this.language = language;
    }

    @Override
    public TokenizedContent apply(SegmentedContent objects) {

        Long now = System.currentTimeMillis();
        List<String> tokens = TextPreProcessor.tokenize(language, objects.getContent());
        JLogger.getLogger(this).info("Tokenization elapsed: " + (System.currentTimeMillis() - now) + " ms");

        return new TokenizedContent(tokens, objects.getId(), objects.getTargets());
    }
}
