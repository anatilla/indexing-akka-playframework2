package it.alessandronatilla.indexing.pipelines.steps.nlp;

import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.indexing.pipelines.steps.nlp.model.ExtractedContent;
import it.alessandronatilla.indexing.rest.model.TextContent;
import play.db.jpa.Transactional;

import java.util.function.Function;

/**
 * Author: alexander
 * Project: akka-es-indexing-service
 */
public class ContentExtractor implements Function<TextContent, ExtractedContent> {

    private final DataStore.Accessor persistence;

    public ContentExtractor(DataStore.Accessor persistence) {
        this.persistence = persistence;
    }


    @Override
    public ExtractedContent apply(TextContent textContent) {

        if (this.persistence.contains(String.valueOf(textContent.getId()))) {
            throw new IllegalArgumentException("Document " + textContent.getId() + " already exists in index " + persistence.getIndexID());
        }
        return new ExtractedContent(textContent.getContent(), textContent.getId(), textContent.getTargets());
    }
}
