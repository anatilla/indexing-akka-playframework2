package it.alessandronatilla.indexing.pipelines.steps.nlp;

import it.alessandronatilla.indexing.pipelines.steps.nlp.model.StopwordLessContent;
import it.alessandronatilla.indexing.pipelines.steps.nlp.model.TaggedContent;
import it.alessandronatilla.preprocessing.TextPreProcessor;
import it.alessandronatilla.preprocessing.model.Language;
import it.alessandronatilla.preprocessing.model.TaggedWord;
import it.alessandronatilla.utils.JLogger;

import java.util.List;
import java.util.function.Function;

/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class PartOfSpeechTagger implements Function<StopwordLessContent, TaggedContent> {

    protected Language language;

    public PartOfSpeechTagger(Language language) {
        this.language = language;
    }

    @Override
    public TaggedContent apply(StopwordLessContent strings) {
        List<TaggedWord> words = null;

        try {
            Long now = System.currentTimeMillis();
            words = TextPreProcessor.tag(language, strings.getContent());
            JLogger.getLogger(this.getClass().getName()).info("POS-Tag elapsed: " + (System.currentTimeMillis() - now) + " ms");
        } catch (Exception e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());        }

        return new TaggedContent(words, strings.getId(), strings.getTargets());
    }
}
