package it.alessandronatilla.indexing.pipelines.steps.nlp;

import it.alessandronatilla.indexing.pipelines.steps.nlp.model.NopunctuatedContent;
import it.alessandronatilla.indexing.pipelines.steps.nlp.model.TokenizedContent;
import it.alessandronatilla.preprocessing.TextPreProcessor;
import it.alessandronatilla.utils.JLogger;

import java.util.List;
import java.util.function.Function;

/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class PunctuationRemover implements Function<TokenizedContent, NopunctuatedContent> {


    @Override
    public NopunctuatedContent apply(TokenizedContent strings) {
        Long now = System.currentTimeMillis();
        List<String> clear_words = TextPreProcessor.remove_punctuation(strings.getContent());

        JLogger.getLogger(this.getClass().getName()).info("RemovePunctuation elapsed: " + (System.currentTimeMillis() - now) + " ms");
        return new NopunctuatedContent(clear_words, strings.getId(), strings.getTargets());
    }
}
