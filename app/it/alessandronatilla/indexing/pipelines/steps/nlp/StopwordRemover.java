package it.alessandronatilla.indexing.pipelines.steps.nlp;

import it.alessandronatilla.indexing.pipelines.steps.nlp.model.NopunctuatedContent;
import it.alessandronatilla.indexing.pipelines.steps.nlp.model.StopwordLessContent;
import it.alessandronatilla.preprocessing.TextPreProcessor;
import it.alessandronatilla.preprocessing.model.Language;
import it.alessandronatilla.utils.JLogger;

import java.util.List;
import java.util.function.Function;

/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class StopwordRemover implements Function<NopunctuatedContent, StopwordLessContent> {

    protected Language language;

    public StopwordRemover(Language language) {
        this.language = language;
    }

    @Override
    public StopwordLessContent apply(NopunctuatedContent strings) {

        Long now = System.currentTimeMillis();
        List<String> clear_words = TextPreProcessor.remove_stopwords(language, strings.getContent());
        JLogger.getLogger(this).info("RemoveStopwords elapsed: " + (System.currentTimeMillis() - now) + " ms");

        return new StopwordLessContent(clear_words, strings.getId(), strings.getTargets());
    }
}
