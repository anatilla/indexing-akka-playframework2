package it.alessandronatilla.indexing.pipelines;

import java.util.function.Function;

/**
 * @param <I> Class specifying input type
 * @param <O> Class specifying output type
 * @author Angelo Impedovo
 *         Defines a step to be computed over an input of type I
 *         which will produce an output of type O.
 *         This class implements the functional interface Function<I,O>
 *         because the step to be computed can be provided by the developer
 *         through the implementation of "apply" method.
 */
public final class Step<I, O> implements Function<I, O> {

    /**
     * Pointer to the next step to be executed after this step
     * completion.
     * The following step will be executed with the output
     * of current step as its input, a generic wildcard is used
     * in order to abstract by the output type of the next step.
     */
    protected Step<O, ?> nextStep;

    /**
     * Functional encapsulated by this step.
     * It is the work to be performed by the apply method.
     */
    private Function<I, O> functional;


    Step(Function<I, O> functional) {
        this.functional = functional;
    }


    /**
     * Applies the functional to an input object of type I
     * producing an output object of type O.
     */
    @Override
    public O apply(I t) {
        return this.functional.apply(t);
    }

}
