package it.alessandronatilla.indexing.messages;

import it.alessandronatilla.indexing.pipelines.Pipeline;
import it.alessandronatilla.indexing.rest.model.TextContent;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class InitMessage<I extends TextContent, O> {
    private final Collection<I> dataset;
    private final Pipeline<I, O> pipeline;
    private final String uuid;
    private List<Work> workloads;

    public InitMessage(Pipeline<I, O> pipelinePrototype, Collection<I> dataset, String uuid) {
        this.pipeline = pipelinePrototype;
        this.uuid = uuid;
        this.dataset = dataset;
        this.workloads = new LinkedList<Work>();

        int counter = 0;
        for (I data : dataset) {
            this.workloads.add(new WorkUnit(data));
        }
    }

    public String getUuid() {
        return uuid;
    }

    public Collection<I> getDataset() {
        return this.dataset;
    }

    public Pipeline<I, O> getPipeline() {
        return this.pipeline;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Calculate{");
        sb.append("pipeline=").append(this.pipeline);
        sb.append(", dataset=").append(this.dataset);
        sb.append('}');
        return sb.toString();
    }


    public Stream<Work> getStream() {
        return this.workloads.stream();
    }


    private class WorkUnit implements Work {
        I data;

        private WorkUnit(I data) {
            this.data = data;
        }

        @Override
        public Object doWork() {
            return InitMessage.this.pipeline.apply(this.data);
        }

        @Override
        public String getId() {
            return this.data.getId();
        }
    }

}
