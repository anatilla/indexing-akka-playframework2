package it.alessandronatilla.indexing.messages;

/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class WorkMessage {

    private final Work unitOfWork;
    private final String uuid;

    public WorkMessage(Work unitOfWork, String uuid) {
        this.uuid = uuid;
        this.unitOfWork = unitOfWork;

    }

    public String getUuid() {
        return uuid;
    }

    public Work getUnitOfWork() {
        return this.unitOfWork;
    }
}
