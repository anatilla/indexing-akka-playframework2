package it.alessandronatilla.indexing.messages;

public interface Work {
    Object doWork();

    String getId();
}
