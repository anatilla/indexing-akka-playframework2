package it.alessandronatilla.indexing.messages;

/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class ResultMessage {
    private final Object result;
    private final String uuid;
    private final String work_id;
    private final Boolean succeeded;


    public ResultMessage(Object result, String uuid, String work_id, boolean succeeded) {
        this.uuid = uuid;
        this.result = result;
        this.work_id = work_id;
        this.succeeded = succeeded;
    }

    public String getWork_id() {
        return work_id;
    }

    public String getUuid() {
        return uuid;
    }

    public Object getResult() {
        return this.result;
    }

    public Boolean isSucceeded() {
        return this.succeeded;
    }

    @Override
    public String toString() {
        return "StandardPipelineResultMessage{" + "result=" + result + '}';
    }


}
