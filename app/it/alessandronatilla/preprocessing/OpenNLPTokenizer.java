package it.alessandronatilla.preprocessing;

import it.alessandronatilla.preprocessing.model.Language;
import it.alessandronatilla.utils.JLogger;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.InvalidFormatException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Author: alexander
 * Project: text-preprocessor
 */
public class OpenNLPTokenizer {

    private static Map<Language, OpenNLPTokenizer> register;
    private static boolean already_initialized = false;
    private Tokenizer tokenizer = null;
    private SentenceDetectorME sentenceDetector = null;
    private Language language;

    private OpenNLPTokenizer(Language language) {

        JLogger.getLogger(this.getClass().getName()).info("Initializing " + this.getClass().getName() + ", lang = " + language);
        sentenceDetector = loadSentenceModel(language);
        tokenizer = loadTokenizerModel(language);
    }

    public static void initialize() {
        if (!already_initialized) {
            register = new HashMap<>();
            OpenNLPTokenizer italian = new OpenNLPTokenizer(Language.IT);
            OpenNLPTokenizer english = new OpenNLPTokenizer(Language.EN);
            register.put(Language.IT, italian);
            register.put(Language.EN, english);
            already_initialized = true;
        }
    }

    public static OpenNLPTokenizer getInstance(Language language) {
        if (register == null)
            initialize();
        return register.get(language);
    }

    private Tokenizer loadTokenizerModel(Language language) {
        InputStream modelIn = null;
        Tokenizer tokenizer = null;

        if (language.equals(Language.IT)) {
            modelIn = getClass().getResourceAsStream("/it-token.bin");
        } else if (language.equals(Language.EN)) {
            modelIn = getClass().getResourceAsStream("/en-token.bin");
        }

        try {
            tokenizer = new TokenizerME(new TokenizerModel(modelIn));
        } catch (FileNotFoundException | InvalidFormatException e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
        } catch (IOException e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
        } finally {
            try {
                modelIn.close();
            } catch (IOException e) {
                JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
            }
        }

        return tokenizer;
    }

    private SentenceDetectorME loadSentenceModel(Language language) {
        SentenceDetectorME model = null;
        InputStream modelIn = null;

        if (language.equals(Language.IT)) {
            modelIn = getClass().getResourceAsStream("/it-sent.bin");
        } else if (language.equals(Language.EN)) {
            modelIn = getClass().getResourceAsStream("/en-sent.bin");
        }

        try {
            model = new SentenceDetectorME(new SentenceModel(modelIn));
        } catch (FileNotFoundException | InvalidFormatException e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
        } catch (IOException e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
        } finally {
            try {
                modelIn.close();
            } catch (IOException e) {
                JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
            }
        }

        return model;

    }

    public synchronized List<String> tokenize(List<String> sentences, Language language) {
        List<String> tokens_list = new LinkedList<String>();
        for (String sentence : sentences) {
            String[] tokens = register.get(language).tokenizer.tokenize(sentence);

            for (String token : tokens) {
                tokens_list.add(token.toLowerCase());
            }
        }

        return tokens_list;
    }

    public synchronized List<String> get_sentences(String text, Language language) {
        String[] sentences = register.get(language).sentenceDetector.sentDetect(text);
        List<String> result = new LinkedList<>();

        Collections.addAll(result, sentences);

        return result;
    }
}
