package it.alessandronatilla.preprocessing.model;

/**
 * Author: alexander
 * Project: text-preprocessor
 */
public class StemmedWord extends TaggedWord {

    /**
     * The stem of the tokens
     */
    protected String stem;

    public StemmedWord(TaggedWord tagWord, String stem) {
        super(tagWord.token, tagWord.posTag, tagWord.lemma);
        this.stem = stem;
    }

    protected StemmedWord() {
        super();
    }

    /**
     * Returns the stem of this stemmedWord
     *
     * @return stem
     */
    public String getStem() {
        return stem;
    }

    /**
     * Set the stem of this stemmedWord
     *
     * @param stem stem of the tokens
     */
    public void setStem(String stem) {
        this.stem = stem;
    }

    @Override
    public String toString() {
        return "StemmedWord [ tokens=" + token + ", posTag=" + posTag
                + ", lemma=" + lemma + ", stem=" + stem + "]";
    }

}
