package it.alessandronatilla.preprocessing.model;

/**
 * Author: alexander
 * Project: text-preprocessor
 */
public enum Language {
    IT, EN
}
