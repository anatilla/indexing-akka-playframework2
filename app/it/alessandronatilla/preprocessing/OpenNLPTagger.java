package it.alessandronatilla.preprocessing;

import it.alessandronatilla.preprocessing.exceptions.UnsupportedLanguageException;
import it.alessandronatilla.preprocessing.model.Language;
import it.alessandronatilla.preprocessing.model.TaggedWord;
import it.alessandronatilla.utils.JLogger;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.util.InvalidFormatException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: alexander
 * Project: text-preprocessor
 */
public class OpenNLPTagger {

    private static Map<Language, OpenNLPTagger> register;
    private static boolean already_initialized = false;
    private POSModel model;
    private POSTaggerME tagger;
    private Language language;

    private OpenNLPTagger(Language language) {
        JLogger.getLogger(this.getClass().getName()).info("Initializing " + this.getClass().getName() + ", lang = " + language);
        this.language = language;
        InputStream modelIn = null;

        try {
            if (this.language.equals(Language.IT)) {
                modelIn = OpenNLPTagger.class
                        .getResourceAsStream("/it-pos_perceptron.bin");
                model = new POSModel(modelIn);
                tagger = new POSTaggerME(model);
            } else if (this.language.equals(Language.EN)) {
                modelIn = OpenNLPTagger.class
                        .getResourceAsStream("/en-pos-perceptron.bin");
                model = new POSModel(modelIn);
                tagger = new POSTaggerME(model);
            } else throw new UnsupportedLanguageException();
        } catch (FileNotFoundException | InvalidFormatException e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
        } catch (IOException e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
        } finally {
            try {
                modelIn.close();
            } catch (IOException e) {
                JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
            }
        }
    }

    public static void initialize() {
        if (!already_initialized) {
            register = new HashMap<>();

            OpenNLPTagger italian = new OpenNLPTagger(Language.IT);
            OpenNLPTagger english = new OpenNLPTagger(Language.EN);
            register.put(Language.IT, italian);
            register.put(Language.EN, english);
            already_initialized = true;
        }
    }

    public static OpenNLPTagger getInstance(Language language) {
        if (register == null)
            initialize();
        return register.get(language);
    }

    public List<TaggedWord> tag(List<String> tokenizedText, Language language) throws Exception {
        String[] tokens = tokenizedText.toArray(new String[1]);

        List<TaggedWord> taggedWords = new ArrayList<TaggedWord>();
        for (int i = 0; i < tokens.length; i++) {
            try {
                taggedWords.add(tag(tokens[i], language));
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }

        return taggedWords;
    }

    public TaggedWord tag(String token, Language language) throws Exception {
        if (token.matches("( )+") || token.isEmpty())
            throw new Exception("The string is empty or contains spaces only");

        String[] tokens = new String[]{token};
        String[] tags = register.get(language).tagger.tag(tokens);

        if (tags.length == 0)
            throw new Exception("Sorry, there are no tags for this string!");

        String tag = tags[0];
        return new TaggedWord(token, tag, TextPreProcessor.lemmatize(language, token, tag));
    }


}
