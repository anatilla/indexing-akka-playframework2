package it.alessandronatilla.preprocessing;

import it.alessandronatilla.preprocessing.model.Language;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Author: alexander
 * Project: text-preprocessor
 */
class SentenceSegmentator {

    /**
     * Returns all sentences detected in the text as a list of separated string.
     * Works only for English and Italian
     *
     * @param text raw text
     * @return the sentences in the text with the dot, question mark, bang and
     * so on at the end
     */
    public static List<String> segment(Language language, String text) {

        Locale locale = null;
        if (language.equals(Language.IT))
            locale = Locale.ITALIAN;
        else if (language.equals(Language.EN))
            locale = Locale.ENGLISH;

        List<String> sentences = new ArrayList<String>();

        BreakIterator boundary = BreakIterator
                .getSentenceInstance(locale);
        boundary.setText(text);

        int start = boundary.first();
        for (int end = boundary.next(); end != BreakIterator.DONE; start = end, end = boundary
                .next()) {
            sentences.add(text.substring(start, end));
        }

        return sentences;
    }
}
