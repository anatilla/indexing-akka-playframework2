package it.alessandronatilla.preprocessing;

import it.alessandronatilla.preprocessing.exceptions.UnsupportedLanguageException;
import it.alessandronatilla.preprocessing.model.Language;
import it.alessandronatilla.preprocessing.model.StemmedWord;
import it.alessandronatilla.preprocessing.model.TaggedWord;
import org.tartarus.snowball.ext.EnglishStemmer;
import org.tartarus.snowball.ext.ItalianStemmer;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: alexander
 * Project: text-preprocessor
 */
class WordStemmerSingleton {

    private static WordStemmerSingleton self;
    private final Language language;
    private ItalianStemmer italianLightStemmer;
    private EnglishStemmer englishStemmer;

    private WordStemmerSingleton(Language language) {
        this.language = language;
        if (language.equals(Language.IT))
            italianLightStemmer = new ItalianStemmer();

        else if (language.equals(Language.EN))
            englishStemmer = new EnglishStemmer();

        else throw new UnsupportedLanguageException("Language not supported");
    }

    public static WordStemmerSingleton getIstance(Language language) {
        if (self == null)
            self = new WordStemmerSingleton(language);

        return self;
    }

    /**
     * Returns the stem of the single tokens given in input.
     *
     * @param token a SINGLE tokens
     * @return stem of tokens
     */
    public synchronized String stem(String token) {
        if (this.language.equals(Language.IT)) {
            italianLightStemmer.setCurrent(token);
            italianLightStemmer.stem();
            return italianLightStemmer.getCurrent();

        } else if (this.language.equals(Language.EN)) {
            englishStemmer.setCurrent(token);
            englishStemmer.stem();
            return englishStemmer.getCurrent();
        } else throw new UnsupportedLanguageException("Language not supported");
    }

    /**
     * Makes the stem of each TaggedWord given in input, and returns a list of
     * StemmedWord
     *
     * @param tagged list of TaggedWord
     * @return list of StemmedWord
     */
    public List<StemmedWord> stem(List<TaggedWord> tagged) {

        List<StemmedWord> list = new ArrayList<StemmedWord>();
        for (TaggedWord tag : tagged) {
            String stem = stem(tag.getToken());
            list.add(new StemmedWord(tag, stem));
        }

        return list;
    }

    public StemmedWord stem(TaggedWord taggedWord) {
        return new StemmedWord(taggedWord, stem(taggedWord.getToken()));
    }

}
