package it.alessandronatilla.preprocessing.lemmatizer.it;


import it.alessandronatilla.utils.JLogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author pierpaolo
 */
class ItalianLemmatizer {

    private static boolean init = false;

    private static Map<String, String> dict;

    public static void init(InputStream dictFile, InputStream posTagConversionFile) throws Exception {
        JLogger.getLogger(ItalianLemmatizer.class).info("Initializing " + ItalianLemmatizer.class.getName());

        Map<String, Set<String>> postagMap = buildMorpht2TanlPosTagMap(posTagConversionFile);
        loadDict(dictFile, postagMap);
        init = true;
    }

    public static String[] lemmatize(InputStream dictFile, InputStream posTagConversionFile, String[] tokens, String[] posTag) throws Exception {

        if (!init) {
            init(dictFile, posTagConversionFile);
        }

        if (tokens.length != posTag.length) {
            throw new IllegalArgumentException("Tokens and pos-tags with different size");
        }

        String[] lemmas = new String[tokens.length];

        for (int i = 0; i < tokens.length; i++) {
            String lemma = dict.get(tokens[i] + "_" + posTag[i]);

            if (lemma == null) {
                String failsafeTagTanl = getFailsafeTagTanl(posTag[i]);

                if (failsafeTagTanl != null) {
                    lemma = dict.get(tokens[i] + "_#" + failsafeTagTanl + "#");

                } else {
                    lemma = dict.get(tokens[i] + "_#!#");

                }
                if (lemma == null) {
                    lemma = tokens[i].toLowerCase();
                }
            }
            lemmas[i] = lemma;
        }
        return lemmas;
    }

    private static String getFailsafeTagTanl(String tag) {
        if (tag.startsWith("S")) {
            return "N";
        } else if (tag.startsWith("V")) {
            return "V";
        } else if (tag.startsWith("B")) {
            return "R";
        } else if (tag.startsWith("A") || tag.startsWith("NO")) {
            return "A";
        }
        return null;
    }

    private static String getFailsafeTagMorpthIt(String tag) {
        if (tag.contains("NOUN")) {
            return "N";
        } else if (tag.contains("VER") || tag.contains("AUX") || tag.contains("MOD")) {
            return "V";
        } else if (tag.contains("ADV")) {
            return "R";
        } else if (tag.contains("ADJ")) {
            return "A";
        }
        return null;
    }

    private static void loadDict(InputStream dictFile, Map<String, Set<String>> posTagMap) throws IOException {
        dict = new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(dictFile, "UTF-8"));
        String line;

        while (reader.ready()) {
            line = reader.readLine();
            String[] split = line.split("\\s+");
            if (split.length == 3) {
                Set<String> set = posTagMap.get(split[2]);
                if (set != null) {
                    for (String p : set) {
                        dict.put(split[0] + "_" + p, split[1]);
                    }
                } else {
                    String failsafeTag = getFailsafeTagMorpthIt(split[2]);
                    if (failsafeTag != null) {
                        dict.put(split[0] + "_#" + failsafeTag + "#", split[1]);
                    } else {
                        dict.put(split[0] + "_#!#", split[1]);
                    }
                }
            } else {
                throw new IOException("No valid dict file, line: " + line);
            }
        }
        reader.close();

    }

    private static Map<String, Set<String>> buildMorpht2TanlPosTagMap(InputStream file) throws IOException {
        Map<String, Set<String>> map = new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(file, "UTF-8"));
        String line;

        while (reader.ready()) {
            line = reader.readLine();
            String[] posSplit = line.split("\t");

            if (posSplit.length == 2) {
                String[] morphPosSplit = posSplit[1].split(" ");

                for (String mp : morphPosSplit) {
                    Set<String> set = map.get(mp);

                    if (set == null) {
                        set = new HashSet<>();
                        map.put(mp, set);
                    }
                    set.add(posSplit[0]);
                }
            } else {
                throw new IOException("No valid pos tag conversion file, line: " + line);
            }
        }

        reader.close();
        return map;
    }

}
