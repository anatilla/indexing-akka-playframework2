package it.alessandronatilla.preprocessing.lemmatizer.it;

import it.alessandronatilla.utils.JLogger;

import java.io.InputStream;

/**
 * Author: alexander
 * Project: crowd-pulse
 */
public class LemmatizerITSingleton {

    private static LemmatizerITSingleton instance = null;
    private final String dict_fname = "/lemmatization/morph-it_048.txt";
    private final String tagset_fname = "/lemmatization/tanl_morph-it";
    private boolean init = false;

    private LemmatizerITSingleton() {
        InputStream dict = LemmatizerITSingleton.class.getResourceAsStream(dict_fname);
        InputStream tagset = LemmatizerITSingleton.class.getResourceAsStream(tagset_fname);

        try {
            ItalianLemmatizer.init(dict, tagset);
        } catch (Exception e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());        }
        init = true;
    }

    public static LemmatizerITSingleton getInstance() {
        if (instance == null) {
            instance = new LemmatizerITSingleton();
        }

        return instance;
    }

    public String lemmatizer(String token, String posTag) {

        if (!init) {
            new LemmatizerITSingleton();
            init = true;
        }
        String lemma = null;

        InputStream dict = LemmatizerITSingleton.class.getResourceAsStream(dict_fname);
        InputStream tagset = LemmatizerITSingleton.class.getResourceAsStream(tagset_fname);

        try {
            lemma = ItalianLemmatizer.lemmatize(dict, tagset, new String[]{token}, new String[]{posTag})[0];
            init = true;
        } catch (Exception e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
        }

        return lemma;
    }


}
