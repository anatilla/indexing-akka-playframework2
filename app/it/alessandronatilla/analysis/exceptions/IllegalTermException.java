package it.alessandronatilla.analysis.exceptions;

/**
 * Created by alexander on 05/05/15.
 */
public class IllegalTermException extends RuntimeException {
    public IllegalTermException(String s) {
        super(s);
    }
}
