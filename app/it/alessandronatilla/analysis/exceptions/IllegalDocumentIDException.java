package it.alessandronatilla.analysis.exceptions;

/**
 * Created by alexander on 05/05/15.
 */
public class IllegalDocumentIDException extends RuntimeException {
    public IllegalDocumentIDException(String message) {
        super(message);
    }
}
