package it.alessandronatilla.analysis.model.split;

import java.util.List;

/**
 * Created by alexander on 15/05/15.
 */
public class AdHocSplitProperty {
    private String corpusID;
    private String splitID;
    private List<String> trainIdList;
    private List<String> testIdList;

    public AdHocSplitProperty() {
    }


    public AdHocSplitProperty(String corpusID, String splitID, List<String> trainIdList, List<String> testIdList) {
        this.corpusID = corpusID;
        this.splitID = splitID;
        this.trainIdList = trainIdList;
        this.testIdList = testIdList;
    }

    public String getCorpusID() {
        return corpusID;
    }

    public String getSplitID() {
        return splitID;
    }

    public List<String> getTrainIdList() {
        return trainIdList;
    }

    public List<String> getTestIdList() {
        return testIdList;
    }


    @Override
    public String toString() {
        return "AdHocSplitProperty{" +
                "corpusID='" + corpusID + '\'' +
                ", trainIdList=" + trainIdList +
                ", testIdList=" + testIdList +
                '}';
    }
}
