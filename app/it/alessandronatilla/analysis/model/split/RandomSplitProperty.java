package it.alessandronatilla.analysis.model.split;

/**
 * Created by alexander on 15/05/15.
 */
public class RandomSplitProperty {
    private String corpusId;
    private String splitId;
    private Long seed;
    private Integer testSetSize;

    public RandomSplitProperty() {
    }

    public RandomSplitProperty(String corpusId, Integer testSetSize, Long seed, String splitId) {
        this.corpusId = corpusId;
        this.testSetSize = testSetSize;
        this.seed = seed;
        this.splitId = splitId;
    }

    public Integer getTestSetSize() {
        return testSetSize;
    }

    public String getCorpusId() {
        return corpusId;
    }

    public String getSplitId() {
        return splitId;
    }

    public Long getSeed() {
        return seed;
    }

    @Override
    public String toString() {
        return "RandomSplitProperty{" +
                "corpusId='" + corpusId + '\'' +
                ", splitId=" + splitId +
                ", seed=" + seed +
                '}';
    }
}
