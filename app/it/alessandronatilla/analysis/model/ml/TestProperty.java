package it.alessandronatilla.analysis.model.ml;

/**
 * Created by alexander on 13/05/15.
 */
public class TestProperty extends ClassificationProperty {

    private String splitId;

    public TestProperty(String modelName, String corpusId, String splitId) {
        super(modelName, corpusId);
        this.splitId = splitId;
    }

    public TestProperty() {
    }

    public String getSplitId() {
        return splitId;
    }
}
