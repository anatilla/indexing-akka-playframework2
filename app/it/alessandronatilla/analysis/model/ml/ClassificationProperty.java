package it.alessandronatilla.analysis.model.ml;


public class ClassificationProperty {

    protected String modelId;
    protected String corpusId;


    public ClassificationProperty(String modelName, String corpusId) {
        this.modelId = modelName;
        this.corpusId = corpusId;
    }

    public ClassificationProperty() {
    }


    public String getCorpusId() {
        return corpusId;
    }


    public void setCorpusId(String corpusId) {
        this.corpusId = corpusId;
    }


    public String getModelId() {
        return modelId;
    }


    public void setModelName(String modelName) {
        this.modelId = modelName;
    }

}
