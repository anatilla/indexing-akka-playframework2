package it.alessandronatilla.analysis.model.ml;

import it.alessandronatilla.analysis.model.matrix.IndexedField;

public class TrainProperty extends ClassificationProperty {

    private String labelAttribute; //se null l'algoritmo è unsupervised
    private IndexedField trainField;
    private String description;
    private String algorithmName;
    private String splitId;

    public TrainProperty(String modelName, String description,
                         String algorithmName, String corpusId, String labelAttribute,
                         String splitId, IndexedField trainField) {

        super(modelName, corpusId);
        this.splitId = splitId;
        this.algorithmName = algorithmName;
        this.description = description;
        this.labelAttribute = labelAttribute;
        this.trainField = trainField;
    }

    public TrainProperty() {

    }

    public IndexedField getTrainField() {
        return trainField;
    }

    public String getLabelAttribute() {
        return labelAttribute;
    }

    public void setLabelAttribute(String labelAttribute) {
        this.labelAttribute = labelAttribute;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlgorithmName() {
        return algorithmName;
    }

    public void setAlgorithmName(String algorithmName) {
        this.algorithmName = algorithmName;
    }

    public String getSplitId() {
        return splitId;
    }
}
