package it.alessandronatilla.analysis.model.matrix;

/**
 * Created by alexander on 01/05/15.
 */
public enum IndexedField {
    lemmas, stems, postagtoken, tokens
}
