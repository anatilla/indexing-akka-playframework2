package it.alessandronatilla.analysis.model.algorithms;

public enum Algorithm {
	NAIVE_BAYES, LOGISTIC_REGRESSION, DECISION_TREES, LINEAR_SVM, RANDOM_FOREST, K_MEANS
}
