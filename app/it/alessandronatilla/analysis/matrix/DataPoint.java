package it.alessandronatilla.analysis.matrix;

import org.apache.spark.mllib.regression.LabeledPoint;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by alexander on 11/05/15.
 */
public class DataPoint implements Serializable {
    private String doc_id;
    private LabeledPoint point;

    public DataPoint(String doc_id, LabeledPoint point) {
        this.doc_id = doc_id;
        this.point = point;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public LabeledPoint getPoint() {
        return point;
    }

    @Override
    public String toString() {
        return "DataPoint{" +
                "doc_id=" + doc_id +
                ", point=" + point +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DataPoint)) return false;
        DataPoint dataPoint = (DataPoint) o;
        return Objects.equals(getDoc_id(), dataPoint.getDoc_id());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDoc_id());
    }
}
