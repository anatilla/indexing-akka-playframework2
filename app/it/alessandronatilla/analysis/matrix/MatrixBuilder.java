package it.alessandronatilla.analysis.matrix;

import it.alessandronatilla.analysis.model.matrix.IndexedField;
import it.alessandronatilla.analysis.weights.BM25Weight;
import it.alessandronatilla.analysis.weights.TFIDFWeight;
import it.alessandronatilla.analysis.weights.TFWeight;
import it.alessandronatilla.analysis.weights.Weight;
import it.alessandronatilla.analysis.exceptions.IllegalDocumentIDException;
import it.alessandronatilla.analysis.exceptions.IllegalTermException;
import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.indexing.rest.model.WeightScheme;
import it.alessandronatilla.utils.JLogger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by alexander on 02/05/15.
 */
public class MatrixBuilder {

    private Weight weight;
    private IndexReader ireader;
    private DataStore.Accessor accessor;
    private IndexedField indexedField;

    public MatrixBuilder(DataStore.Accessor accessor, IndexedField indexedField) {
        this.accessor = accessor;
        this.ireader = accessor.getReader();
        this.indexedField = indexedField;
        this.weight = this.getWeightSchema();

    }

    private Weight getWeightSchema() {
        WeightScheme schema = accessor.getWeightScheme();
        Weight weightFunc;

        if (schema.equals(WeightScheme.BM25)) {
            weightFunc = new BM25Weight(ireader, indexedField);
        } else if (schema.equals(WeightScheme.TFIDF)) {
            weightFunc = new TFIDFWeight(ireader, indexedField);
        } else if (schema.equals(WeightScheme.TF)) {
            weightFunc = new TFWeight(ireader, indexedField);
        } else weightFunc = null;

        return weightFunc;
    }

    public List<DataPoint> make(String labelField) {
        List<DataPoint> points = new LinkedList<>();
        Map<String, Integer> termIdMap = weight.getTermIdMap();

        for (int i = 0; i < ireader.numDocs(); i++) {
            try {
                Document d = ireader.document(i);
                String lbl = d.get(labelField);
                if (lbl == null) continue;

                String doc_id = d.get("id");
                String[] terms = d.get(indexedField.name()).split("\\s+");
                double[] vector = new double[termIdMap.size()];

                for (String term : terms) {
                    if(term.equals("")) continue;

                    Double w;

                    try {
                        w = weight.local(term, doc_id) * weight.global(term) * weight.norm(doc_id);
                        int term_pos = termIdMap.get(term);
                        vector[term_pos] = w;

                    } catch (IllegalDocumentIDException | IllegalTermException e) {
                        JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
                    }
                }

                LabeledPoint point = new LabeledPoint(Double.parseDouble(lbl), Vectors.dense(vector));
                points.add(new DataPoint(doc_id, point));

            } catch (IOException e) {
                JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
            }
        }

        return points;
    }
}
