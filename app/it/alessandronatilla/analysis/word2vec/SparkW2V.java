package it.alessandronatilla.analysis.word2vec;

import it.alessandronatilla.analysis.model.matrix.IndexedField;
import it.alessandronatilla.analysis.spark.SparkProvider;
import it.alessandronatilla.properties.PropertiesLoader;
import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.utils.JLogger;
import it.alessandronatilla.utils.TimeProvider;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.feature.Word2Vec;
import org.apache.spark.mllib.feature.Word2VecModel;
import scala.Tuple2;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * Created by alexander on 26/05/15.
 */
public class SparkW2V {

    private IndexReader ireader;
    private DataStore.Accessor accessor;
    private IndexedField field;
    private JavaSparkContext sc;
    private Word2VecModel model;
    private String session;

    public SparkW2V(DataStore.Accessor accessor, IndexedField field) {
        this.accessor = accessor;
        this.field = field;
        this.ireader = this.accessor.getReader();
        this.sc = SparkProvider.getInstance().getContext();
        this.session = accessor.getIndexID() + ":" + "word2vec:" + UUID.randomUUID().toString();
        this.build();
    }

    private void build() {
        TimeProvider timeProvider = TimeProvider.getInstance();
        timeProvider.create(session);
        timeProvider.start(session);

        List<List<String>> docs = new LinkedList<>();
        for (int i = 0; i < ireader.numDocs(); i++) {
            try {
                Document d = ireader.document(i);
                String[] terms = d.get(field.name()).split("\\s+");
                List<String> doc = Arrays.asList(terms);
                docs.add(doc);
            } catch (IOException e) {
                JLogger.getLogger(this).error(e.getMessage());
                e.printStackTrace();
            }
        }

        JavaRDD<List<String>> documents = sc.parallelize(docs);
        documents.cache();
        Word2Vec vec = new Word2Vec();
        vec.setMinCount(1);

        Integer maxiter = 0;

        try {
            maxiter = Integer.valueOf(PropertiesLoader.load().getProperty("spark.w2v.maxiter"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        vec.setNumPartitions(maxiter);
        vec.setNumIterations(maxiter);


        this.model = vec.fit(documents);
        JLogger.getLogger(this).info("W2V on " + accessor.getIndexID() + " take " + timeProvider.getElapsed(session) + " ms");
    }

    public List<W2VResult> query(String term, int numResults) {
        List<W2VResult> w2VResults = new LinkedList<>();
        Tuple2<String, Object>[] res = this.model.findSynonyms(term, numResults);
        for (Tuple2<String, Object> tuple : res) {
            w2VResults.add(new W2VResult(tuple._1(), tuple._2()));
        }
        return w2VResults;
    }


}
