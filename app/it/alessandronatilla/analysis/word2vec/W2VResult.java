package it.alessandronatilla.analysis.word2vec;

/**
 * Created by alexander on 06/06/15.
 */
public class W2VResult {
    private String term;
    private Object score;

    public W2VResult(String s, Object o) {
        this.term = s;
        this.score = o;
    }

    public Object getScore() {
        return score;
    }

    public String getTerm() {
        return term;
    }
}
