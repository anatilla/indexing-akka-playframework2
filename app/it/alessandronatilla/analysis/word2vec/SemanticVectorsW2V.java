package it.alessandronatilla.analysis.word2vec;

import it.alessandronatilla.analysis.model.matrix.IndexedField;
import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.utils.JLogger;
import pitt.search.semanticvectors.BuildIndex;
import pitt.search.semanticvectors.Search;

import java.io.File;
import java.util.List;
import java.util.UUID;

/**
 * Created by alexander on 06/06/15.
 */
public class SemanticVectorsW2V {

    private final DataStore.Accessor accessor;
    private final IndexedField field;
    private final String session;
    private final String path;
    private final Integer minFreq = 1;
    private final Integer maxNonAlphabetChars = -1;
    private String termFilePath;
    private String docFilePath;


    public SemanticVectorsW2V(DataStore.Accessor accessor, IndexedField field) {
        this.accessor = accessor;
        this.field = field;
        this.session = accessor.getIndexID() + ":" + "word2vec:" + UUID.randomUUID().toString();
        this.path = accessor.getFilePath();
        this.build();
    }

    public void build() {

        String[] params = this.prepareBuildParams();
        termFilePath = new StringBuffer().append(path).append(File.separatorChar)
                .append("termvectors")
                .append("-")
                .append(accessor.getIndexID())
                .append("-")
                .append(field.name())
                .append(".bin")
                .toString();
        docFilePath = new StringBuffer().append(path).append(File.separatorChar)
                .append("docvectors")
                .append("-")
                .append(accessor.getIndexID())
                .append("-")
                .append(field.name())
                .append(".bin")
                .toString();

        BuildIndex buildIndex = new BuildIndex();

        try {
            buildIndex.buildIndex(params, termFilePath, docFilePath);
        } catch (Exception e) {
            JLogger.getLogger(this).error(e.getMessage());
            e.printStackTrace();
        }
    }

    private String[] prepareBuildParams() {
        return new String[]{
                "-luceneindexpath", path,
                "-minfrequency", String.valueOf(this.minFreq),
                "-maxnonalphabetchars", String.valueOf(maxNonAlphabetChars),
                "-contentsfields", field.name(),
                "-docidfield", "id"};
    }


    public List<W2VResult> query(String term) {
        String[] params = this.prepareQueryParams(term);
        Search search = new Search();

        return search.query(params);
    }

    private String[] prepareQueryParams(String term) {
        return new String[]{
                "-queryvectorfile", termFilePath,
                term

        };
    }

    public List<W2VResult> queryDocs(String term) {
        String[] params =  new String[]{
                "-queryvectorfile", termFilePath,
                "-searchvectorfile", docFilePath,
                term
        };
        Search search = new Search();

        return search.query(params);
    }




    public void deleteModels() {
        new File(termFilePath).delete();
        new File(docFilePath).delete();

    }

}
