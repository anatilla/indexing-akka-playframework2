package it.alessandronatilla.analysis.spark;

import it.alessandronatilla.utils.JLogger;
import it.alessandronatilla.properties.PropertiesLoader;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;


import java.io.IOException;

/**
 * Created by alexander on 04/05/15.
 */
public class SparkProvider {
    private static SparkProvider ourInstance = new SparkProvider();
    private SparkConf conf;
    private JavaSparkContext context;
    //private SQLContext sqlContext;

    private SparkProvider() {

        try {

            String context = PropertiesLoader.load().getProperty("spark.endpoint");
            this.conf = new SparkConf().setMaster(context).setAppName("akka-indexer");
            this.context = new JavaSparkContext(this.conf);
            //this.sqlContext = new SQLContext(this.context);
        } catch (IOException e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
        }

    }

    public static SparkProvider getInstance() {
        return ourInstance;
    }

    public JavaSparkContext getContext() {
        return context;
    }
    
    /*
    public SQLContext getSQLContext() {
    	return this.sqlContext;
    }*/
}
