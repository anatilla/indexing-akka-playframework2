package it.alessandronatilla.analysis.rest.services.algorithms;

import it.alessandronatilla.analysis.ml.AlgorithmManager;

import java.util.Map;

/**
 * Created by alexander on 4/30/15.
 */

/*@Path("/listAlgorithms")*/
public class ListAvailableAlgorithmService {

    public static Map<String, Object> getData() {
        AlgorithmManager manager = AlgorithmManager.getInstance();
        return manager.getRegisteredEntries();
    }
}
