package it.alessandronatilla.analysis.rest.services.splits;

import it.alessandronatilla.stores.datastore.DataStoreManager;
import it.alessandronatilla.stores.splitstore.SplitStore;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by alexander on 18/05/15.
 */
public class ListSplitService {

    public static Map getData() {
        HashMap<String, Object> results = new HashMap<>();
        SplitStore splitStore = SplitStore.getInstance();
        DataStoreManager manager = DataStoreManager.getInstance();
        Collection<String> indices = manager.getIndicesName();

        indices.forEach(i -> {
            List<String> splits = splitStore.getAll(i).stream()
                    .filter(s -> s.getCorpusId().equals(i))
                    .map(s -> s.getSplitId())
                    .collect(Collectors.toList());
            results.put(i, splits);
        });

        return results;
    }
}
