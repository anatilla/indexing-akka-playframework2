package it.alessandronatilla.analysis.rest.services.splits;

import it.alessandronatilla.analysis.exceptions.IllegalDocumentIDException;
import it.alessandronatilla.analysis.model.split.AdHocSplitProperty;
import it.alessandronatilla.stores.dao.DocumentDAO;
import it.alessandronatilla.stores.dao.TestSetDAO;
import it.alessandronatilla.stores.dao.TrainSetDAO;
import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.stores.datastore.DataStoreManager;
import it.alessandronatilla.stores.datastore.TransactionalDelegate;
import it.alessandronatilla.stores.model.DocumentsEntity;
import it.alessandronatilla.stores.model.SplitsEntity;
import it.alessandronatilla.stores.model.TestsetsEntity;
import it.alessandronatilla.stores.model.TrainsetsEntity;
import it.alessandronatilla.stores.splitstore.SplitStore;
import it.alessandronatilla.utils.JLogger;
import it.alessandronatilla.utils.TimeProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by alexander on 15/05/15.
 */
public class CreateAdHocSplitService {

    public static Map<String, Object> getData(AdHocSplitProperty adHocSplitProperty) {
        Map<String, Object> results = new HashMap<>();
        String corpusId = adHocSplitProperty.getCorpusID();
        String splitId = adHocSplitProperty.getSplitID();
        List<String> trainListId = adHocSplitProperty.getTrainIdList();
        List<String> testListId = adHocSplitProperty.getTestIdList();

        DataStoreManager manager = DataStoreManager.getInstance();
        DataStore store = manager.get(corpusId);

        store.create(new TransactionalDelegate() {
            @Override
            public void onExecute(DataStore.Accessor t) throws Exception {

                DocumentDAO documentDAO = new DocumentDAO();
                try {
                    for (String id : trainListId) {
                        if (!t.contains(id) || documentDAO.findById(Long.valueOf(id)) == null)
                            throw new IllegalDocumentIDException("document " + id + " not found in corpus " + corpusId);
                    }

                    for (String id : testListId) {
                        if (!t.contains(id) || documentDAO.findById(Long.valueOf(id)) == null)
                            throw new IllegalDocumentIDException("document " + id + " not found in corpus " + corpusId);
                    }

                } catch (Exception e) {
                    JLogger.getLogger(this).error(e.getMessage());
                    throw e;
                }


                TimeProvider timeProvider = TimeProvider.getInstance();
                String session = corpusId + ":"
                        + splitId + ":"
                        + UUID.randomUUID().toString();
                timeProvider.create(session);
                timeProvider.start(session);

                SplitStore store = SplitStore.getInstance();

                SplitsEntity split = new SplitsEntity(corpusId, null, splitId);
                store.add(split);

                // ammetto l'overlap fra le due liste, posso voler verificare l'overfitting del classificatore
                for (String s : testListId) {
                    DocumentsEntity document = new DocumentDAO().findByIdAndCorpus(corpusId, s);
                    TestsetsEntity testsetsEntity = new TestsetsEntity(document, split);
                    new TestSetDAO().persist(testsetsEntity);
                }

                for (String p : trainListId) {
                    DocumentsEntity document = new DocumentDAO().findByIdAndCorpus(corpusId, p);
                    TrainsetsEntity trainsetsEntity = new TrainsetsEntity(document, split);
                    new TrainSetDAO().persist(trainsetsEntity);
                }

                store.add(split);

                Long elapsed = timeProvider.getElapsed(session);
                JLogger.getLogger(this).info("Elapsed training time for " + session + ": " + elapsed + " ms");
            }

            @Override
            public void onFailure(Exception e) {
                results.put("status", "error");
                results.put("details", e.toString());
            }

            @Override
            public void onSuccess() {
                results.put("status", "ok");
            }
        }).dispatch();

        return results;
    }


}
