package it.alessandronatilla.analysis.rest.services.splits;

import it.alessandronatilla.stores.dao.TestSetDAO;
import it.alessandronatilla.stores.dao.TrainSetDAO;
import it.alessandronatilla.stores.model.SplitsEntity;
import it.alessandronatilla.stores.splitstore.SplitStore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by alexander on 18/05/15.
 */
public class FindSplitService {
    public static Map getData(String splitId, String corpusId) {

        SplitStore store = SplitStore.getInstance();
        SplitsEntity split = null;
        HashMap<String, Object> result = new HashMap<>();

        try {
            split = store.get(splitId, corpusId);
            result.put("status", "ok");
            result.put(splitId + "-" + corpusId, new SplitView(split));
        } catch (Exception ex) {
            result.put("status", "error");
            result.put("details", "split " + splitId + " doesn't exists.");
        }

        return result;
    }

    public static class SplitView {
        public String splitId;
        public String corpusId;
        public Long seed;
        public List<String> testset;
        public List<String> trainingset;

        public SplitView(SplitsEntity split) {
            this.corpusId = split.getCorpusId();
            this.seed = split.getSeed();
            this.splitId = split.getSplitId();
            this.testset = new TestSetDAO().getTestDocuments(split).stream()
                    .map(t -> t.getDocumentId()).collect(Collectors.toList());
            this.trainingset = new TrainSetDAO().getTrainDocuments(split).stream()
                    .map(t -> t.getDocumentId()).collect(Collectors.toList());
        }
    }
}
