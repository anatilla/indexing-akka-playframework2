package it.alessandronatilla.analysis.rest.services.splits;

import it.alessandronatilla.analysis.model.split.RandomSplitProperty;
import it.alessandronatilla.stores.dao.DocumentDAO;
import it.alessandronatilla.stores.dao.TestSetDAO;
import it.alessandronatilla.stores.dao.TrainSetDAO;
import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.stores.datastore.DataStoreManager;
import it.alessandronatilla.stores.datastore.TransactionalDelegate;
import it.alessandronatilla.stores.model.DocumentsEntity;
import it.alessandronatilla.stores.model.SplitsEntity;
import it.alessandronatilla.stores.model.TestsetsEntity;
import it.alessandronatilla.stores.model.TrainsetsEntity;
import it.alessandronatilla.stores.splitstore.SplitStore;
import it.alessandronatilla.utils.JLogger;
import it.alessandronatilla.utils.TimeProvider;
import play.db.jpa.Transactional;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by alexander on 15/05/15.
 */
public class CreateRandomSplitService {

    @Transactional
    public static Map<String, Object> getData(RandomSplitProperty property) {


        Map<String, Object> results = new HashMap<>();
        String corpusId = property.getCorpusId();
        String splitId = property.getSplitId();
        Long seed = property.getSeed();
        Integer testSetSize = property.getTestSetSize();

        DataStoreManager manager = DataStoreManager.getInstance();
        DataStore store = manager.get(corpusId);
        SplitStore splitStore = SplitStore.getInstance();


        //TODO implement
        store.create(new TransactionalDelegate() {

            private List<String> trainingSet;
            private List<String> testSet;

            @Override
            public void onExecute(DataStore.Accessor t) throws Exception {

                Integer tot_docs = t.listDocuments().size();
                if (property.getTestSetSize() > tot_docs) throw new
                        IllegalArgumentException("testSetSize cannot be > total indexed documents amount");

                TimeProvider timeProvider = TimeProvider.getInstance();
                String session = property.getCorpusId() + ":"
                        + property.getSplitId() + ":"
                        + UUID.randomUUID().toString();

                timeProvider.create(session);
                timeProvider.start(session);

                SplitsEntity split = new SplitsEntity(corpusId, seed, splitId);
                splitStore.add(split);

                String[] indices = t.listDocuments().stream().map(DocumentsEntity::getDocumentId).toArray(String[]::new); //array of documents ID
                String[] sample = pickSample(indices, testSetSize, new Random(seed));

                testSet = Arrays.asList(sample);


                for (String s : testSet) {
                    DocumentsEntity document = new DocumentDAO().findByIdAndCorpus(corpusId, s);
                    TestsetsEntity testsetsEntity = new TestsetsEntity(document, split);
                    new TestSetDAO().persist(testsetsEntity);
                }

                trainingSet = t.listDocuments().stream().map(DocumentsEntity::getDocumentId)
                        .filter(s -> !testSet.contains(s))
                        .collect(Collectors.toList());

                for (String p : trainingSet) {
                    DocumentsEntity document = new DocumentDAO().findByIdAndCorpus(corpusId, p);
                    TrainsetsEntity trainsetsEntity = new TrainsetsEntity(document, split);
                    new TrainSetDAO().persist(trainsetsEntity);
                }

                Long elapsed = timeProvider.getElapsed(session);
                JLogger.getLogger(this).info("Elapsed random split creation time for " + session + ": " + elapsed + " ms");

            }

            @Override
            public void onFailure(Exception e) {
                results.put("status", "error");
                results.put("details", e.toString());
            }

            @Override
            public void onSuccess() {
                results.put("status", "ok");

            }
        }).dispatch();

        return results;
    }

    public static <T> T[] pickSample(T[] population, int nSamplesNeeded, Random r) {
        T[] ret = (T[]) Array.newInstance(population.getClass().getComponentType(),
                nSamplesNeeded);

        int nPicked = 0, i = 0, nLeft = population.length;
        while (nSamplesNeeded > 0) {
            int rand = r.nextInt(nLeft);
            if (rand < nSamplesNeeded) {
                ret[nPicked++] = population[i];
                nSamplesNeeded--;
            }
            nLeft--;
            i++;
        }
        return ret;
    }

}
