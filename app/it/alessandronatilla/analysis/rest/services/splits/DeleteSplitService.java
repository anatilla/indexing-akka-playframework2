package it.alessandronatilla.analysis.rest.services.splits;

import it.alessandronatilla.analysis.exceptions.SplitNotFoundException;
import it.alessandronatilla.stores.splitstore.SplitStore;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexander on 18/05/15.
 */
public class DeleteSplitService {
    public static Map getData(String splitId, String corpusId) {
        SplitStore store = SplitStore.getInstance();
        HashMap<String, Object> res = new HashMap<>();
        try {
            store.delete(corpusId, splitId);
            res.put("status", "ok");
        } catch (Exception e) {
            res.put("status", "error");
            res.put("details", "split " + splitId + " doesn't exists");
        }
        return res;
    }
}
