package it.alessandronatilla.analysis.rest.services.models;


import it.alessandronatilla.stores.model.ModelsEntity;
import it.alessandronatilla.stores.modelstore.ModelStore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*@Path("/listModels")*/
public class ListMLModelsService {
    public static Map<String, List<Map<String, String>>> getResult() {

        HashMap<String, List<Map<String, String>>> results = new HashMap<>();
        ModelStore store = ModelStore.getInstance();
        Stream<ModelsEntity> stream = store.getModelStream();
        //modelId => descriptor

        stream.forEach(entry -> {
            List<Map<String, String>> list = store.getModelStream()
                    .filter(e1 -> e1.getCorpus().getCorpusId().equals(entry.getCorpus().getCorpusId()))
                    .map(ek -> {
                        TreeMap<String, String> out = new TreeMap<>();
                        out.put("modelId", ek.getModelId());
                        out.put("algorithm", ek.getAlgorithm());
                        out.put("description", ek.getDescription());
                        return out;
                    })
                    .collect(Collectors.toList());

            results.put(entry.getCorpus().getCorpusId(), list);
        });

        return results;
    }


}
