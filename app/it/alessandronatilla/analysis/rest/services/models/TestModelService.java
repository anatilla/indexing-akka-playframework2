package it.alessandronatilla.analysis.rest.services.models;

import it.alessandronatilla.analysis.matrix.DataPoint;
import it.alessandronatilla.analysis.matrix.MatrixBuilder;
import it.alessandronatilla.analysis.ml.ClassificationModel;
import it.alessandronatilla.analysis.model.matrix.IndexedField;
import it.alessandronatilla.analysis.model.ml.TestProperty;
import it.alessandronatilla.analysis.spark.SparkProvider;
import it.alessandronatilla.stores.dao.ModelsDao;
import it.alessandronatilla.stores.dao.TrainSetDAO;
import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.stores.datastore.DataStoreManager;
import it.alessandronatilla.stores.datastore.TransactionalDelegate;
import it.alessandronatilla.stores.model.ModelsEntity;
import it.alessandronatilla.stores.model.SplitsEntity;
import it.alessandronatilla.stores.modelstore.ModelStore;
import it.alessandronatilla.stores.splitstore.SplitStore;
import it.alessandronatilla.utils.JLogger;
import it.alessandronatilla.utils.TimeProvider;
import org.apache.spark.api.java.JavaRDD;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by alexander on 4/30/15.
 */

/*@Path("/testModel")*/
public class TestModelService {

    public Map testModel(TestProperty indexProperties) {
        String corpusId = indexProperties.getCorpusId();
        String modelID = indexProperties.getModelId();

        HashMap<String, Object> results = new HashMap<>();
        ModelStore modelStore = ModelStore.getInstance();
        DataStoreManager manager = DataStoreManager.getInstance();
        DataStore dataStore = manager.get(corpusId);

        //occorre verificare che esista un corpus di riferimento
        if (dataStore == null) {
            results.put("status", "error");
            results.put("details", "corpus " + corpusId + " doesn't exists.");
            return results;
        }

        //occorre verificare che non esista già un modello con lo stesso nome
        if (!modelStore.hasModel(modelID)) {
            results.put("status", "error");
            results.put("details", modelID + " not found.");
            return results;
        }

        String splitId = indexProperties.getSplitId();

        if (splitId.isEmpty()) {
            results.put("status", "error");
            results.put("details", "splitId must be non empty in TrainProperty");
            return results;
        }

        SplitStore splitStore = SplitStore.getInstance();
        SplitsEntity split;

        try {
            split = splitStore.get(splitId, corpusId);
        } catch (Exception e) {
            results.put("status", "error");
            results.put("details", "split " + splitId + " referred to " + corpusId + " doesn't exists.");
            return results;
        }


        dataStore.create(new TransactionalDelegate() {
            private Map<String, Object> res = new HashMap<>();

            @Override
            public void onExecute(DataStore.Accessor dataAccessor) {

                TimeProvider timeProvider = TimeProvider.getInstance();
                String session = indexProperties.getModelId() + ":"
                        + indexProperties.getCorpusId() + ":"
                        + indexProperties.getSplitId()
                        + UUID.randomUUID().toString();

                timeProvider.create(session);
                timeProvider.start(session);

                ModelsEntity modelDescriptor = null;
                try {
                    modelDescriptor = new ModelsDao().findById(modelID);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String labelAttribute = modelDescriptor != null ? modelDescriptor.getLabelField() : null;
                IndexedField field = IndexedField.valueOf(modelDescriptor != null ? modelDescriptor.getTrainField() : null);

                List<DataPoint> datasetList = new MatrixBuilder(dataAccessor, field).make(labelAttribute);
                JavaRDD<DataPoint> dataset = SparkProvider.getInstance().getContext().parallelize(datasetList);

                List<String> testSetIds = new TrainSetDAO().getTrainDocuments(split).stream()
                        .map(t -> t.getDocumentId()).collect(Collectors.toList());

                JavaRDD<DataPoint> testSet = dataset.filter(d -> testSetIds.stream()
                                .collect(Collectors.toList())
                                .contains(d.getDoc_id())
                );

                JLogger.getLogger(this).info("testModel set size " + testSet.count());
                ClassificationModel model = modelStore.getModel(modelID);
                res.putAll(model.test(testSet));

                Long elapsed = timeProvider.getElapsed(session);
                JLogger.getLogger(this).info("Elapsed testing time for " + session + ": " + elapsed + " ms");
                res.put("elapsed", elapsed);
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
                results.put("status", "error");
                results.put("details", e.toString());
            }

            @Override
            public void onSuccess() {
                results.put("status", "ok");
                results.put("details", res);
            }
        }).dispatch();

        return results;
    }

}
