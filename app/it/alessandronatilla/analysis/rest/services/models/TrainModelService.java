package it.alessandronatilla.analysis.rest.services.models;

import it.alessandronatilla.analysis.matrix.DataPoint;
import it.alessandronatilla.analysis.matrix.MatrixBuilder;
import it.alessandronatilla.analysis.ml.ClassificationModel;
import it.alessandronatilla.analysis.model.matrix.IndexedField;
import it.alessandronatilla.analysis.model.ml.TrainProperty;
import it.alessandronatilla.analysis.spark.SparkProvider;
import it.alessandronatilla.stores.dao.CorporaDAO;
import it.alessandronatilla.stores.dao.TrainSetDAO;
import it.alessandronatilla.stores.datastore.DataStore;
import it.alessandronatilla.stores.datastore.DataStoreManager;
import it.alessandronatilla.stores.datastore.TransactionalDelegate;
import it.alessandronatilla.stores.model.CorporaEntity;
import it.alessandronatilla.stores.model.ModelsEntity;
import it.alessandronatilla.stores.model.SplitsEntity;
import it.alessandronatilla.stores.modelstore.ModelStore;
import it.alessandronatilla.stores.splitstore.SplitStore;
import it.alessandronatilla.utils.JLogger;
import it.alessandronatilla.utils.TimeProvider;
import org.apache.spark.api.java.JavaRDD;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by alexander on 4/30/15.
 */

public class TrainModelService {

    public Map trainModel(TrainProperty indexProperties, ClassificationModel model) {
        String corpusId = indexProperties.getCorpusId();
        String modelID = indexProperties.getModelId();
        String modelDescription = indexProperties.getDescription();
        String modelAlgorithmName = indexProperties.getAlgorithmName();

        HashMap<String, Object> results = new HashMap<>();
        ModelStore modelStore = ModelStore.getInstance();
        DataStoreManager manager = DataStoreManager.getInstance();
        DataStore dataStore = manager.get(corpusId);

        //occorre verificare che esista un corpus di riferimento
        if (dataStore == null) {
            results.put("status", "error");
            results.put("details", "corpus " + corpusId + " doesn't exists.");
            return results;
        }

        //occorre verificare che non esista già un modello con lo stesso nome
        if (modelStore.hasModel(modelID)) {
            results.put("status", "error");
            results.put("details", modelID + " already existing.");
            return results;
        }

        String splitId = indexProperties.getSplitId();

        if (splitId.isEmpty()) {
            results.put("status", "error");
            results.put("details", "splitId must be non empty in TrainProperty");
            return results;
        }

        SplitStore splitStore = SplitStore.getInstance();
        final SplitsEntity split;

        try {
            split = splitStore.get(splitId, corpusId);
        } catch (Exception e) {
            results.put("status", "error");
            results.put("details", "split " + splitId + " referred to " + corpusId + " doesn't exists.");
            return results;
        }

        // addestra e testa il modello

        dataStore.create(new TransactionalDelegate() {
            private Long elapsed;

            @Override
            public void onExecute(DataStore.Accessor dataAccessor) {

                TimeProvider timeProvider = TimeProvider.getInstance();
                String session = indexProperties.getModelId() + ":" + indexProperties.getCorpusId() + ":" + UUID.randomUUID().toString();
                timeProvider.create(session);
                timeProvider.start(session);

                String labelAttribute = indexProperties.getLabelAttribute();
                IndexedField field = indexProperties.getTrainField();
                List<DataPoint> datasetList = new MatrixBuilder(dataAccessor, field).make(labelAttribute);
                JavaRDD<DataPoint> dataset = SparkProvider.getInstance().getContext().parallelize(datasetList);

                Collection<String> trainSetIds = new TrainSetDAO().getTrainDocuments(split).stream()
                        .map(t -> t.getDocumentId()).collect(Collectors.toList());

                JLogger.getLogger(this).info("initial dataset size " + dataset.count());

                JavaRDD<DataPoint> trainingSet = dataset.filter(d -> trainSetIds.stream()
                                .collect(Collectors.toList())
                                .contains(d.getDoc_id())
                );

                JLogger.getLogger(this).info("training set size " + trainingSet.count());
                trainingSet.cache();

                model.train(trainingSet);
                CorporaDAO corporaDAO = new CorporaDAO();
                CorporaEntity corpus = null;
                try {
                    corpus = corporaDAO.findById(corpusId);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String params = play.libs.Json.toJson(model.getParams()).toString();

                ModelsEntity descriptor = new ModelsEntity(modelAlgorithmName, corpus,
                        modelDescription, labelAttribute, modelID, field.name(), params);
                modelStore.persistModel(model, descriptor);

                this.elapsed = timeProvider.getElapsed(session);
                JLogger.getLogger(this).info("Elapsed training time for " + session + ": " + elapsed + " ms");

            }

            @Override
            public void onFailure(Exception e) {
                results.put("status", "error");
                results.put("details", e.toString());
            }

            @Override
            public void onSuccess() {
                results.put("method", indexProperties.getAlgorithmName());
                results.put("modelId", indexProperties.getModelId());
                results.put("elapsed", elapsed);
                results.put("status", "ok");

            }
        }).dispatch();

        return results;
    }


}
