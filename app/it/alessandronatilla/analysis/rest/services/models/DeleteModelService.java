package it.alessandronatilla.analysis.rest.services.models;

import it.alessandronatilla.stores.modelstore.ModelStore;
import it.alessandronatilla.utils.JLogger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexander on 13/05/15.
 */
public class DeleteModelService {

    public static Map getData(String modelId) {
        Map<String, Object> results = new HashMap<>();
        ModelStore store = ModelStore.getInstance();
        try {
            store.delete(modelId);
            results.put("status", "ok");
            results.put("details", "model " + modelId + " successfully deleted");
        } catch (IOException e) {
            results.put("status", "error");
            results.put("details", e.getMessage());
            JLogger.getLogger(DeleteModelService.class).error(e.getMessage());
        }

        return results;
    }
}
