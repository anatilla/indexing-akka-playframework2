package it.alessandronatilla.analysis.ml;

import it.alessandronatilla.analysis.matrix.DataPoint;
import org.apache.spark.api.java.JavaRDD;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * Created by alexander on 04/05/15.
 */

public interface ClassificationModel extends Serializable {
    void train(JavaRDD<DataPoint> trainData);

    Map<String, Object> test(JavaRDD<DataPoint> testData);
    void load(String fpath) throws IOException;
    void save(String fpath);
    Serializable getParams();
    
}
