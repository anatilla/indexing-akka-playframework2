package it.alessandronatilla.analysis.ml;

import it.alessandronatilla.analysis.exceptions.AlgorithmNotFoundException;
import it.alessandronatilla.utils.JLogger;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AlgorithmManager {

    public static AlgorithmManager instance;

    public static AlgorithmManager getInstance() {
        if (instance == null) {
            instance = new AlgorithmManager();
        }
        return instance;
    }

    private HashMap<String, Algorithm<?>> algorithmBasket;

    private AlgorithmManager() {
        this.algorithmBasket = new HashMap<>();
    }

    public void registerAlgorithm(String key, Algorithm<?> algorithm) {
        this.algorithmBasket.put(key, algorithm);
    }

    public Algorithm<?> getAlgorithm(String key) throws AlgorithmNotFoundException {
        Algorithm<?> value = this.algorithmBasket.get(key);
        if (value == null) {
            throw new AlgorithmNotFoundException();
        }

        return value;
    }

    public Map<String, Object> getRegisteredEntries() {
        Map<String, Object> result = new HashMap<>();
        Set<String> keyset = this.algorithmBasket.keySet();
        keyset.forEach(k -> {
                    Algorithm<?> alg = algorithmBasket.get(k);
                    try {
                        result.put(k, alg.getPojoParamClass().newInstance());
                    } catch (InstantiationException e) {
                        JLogger.getLogger(this).error(e.getMessage());
                    } catch (IllegalAccessException e) {
                        JLogger.getLogger(this).error(e.getMessage() );
                    }
                }
        );
        return result;
    }
}
