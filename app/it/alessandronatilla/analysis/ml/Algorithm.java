package it.alessandronatilla.analysis.ml;

import java.io.Serializable;


public abstract class Algorithm<T extends Serializable> implements Serializable {

    public abstract ClassificationModel buildClassifier(T params);

    public abstract Class<T> getPojoParamClass();

}
