package it.alessandronatilla.analysis.ml.algorithms;

import java.io.Serializable;

public class LogisticRegressionParams implements Serializable {

	private Integer classCount = 2;
	
	public LogisticRegressionParams(){
		
	}

	public LogisticRegressionParams(Integer classCount) {
		super();
		this.classCount = classCount;
	}

	public Integer getClassCount() {
		return classCount;
	}

	public void setClassCount(Integer classCount) {
		this.classCount = classCount;
	}

	@Override
	public String toString() {
		return "LogisticRegressionParams [classCount=" + classCount + "]";
	}
	
	
}
