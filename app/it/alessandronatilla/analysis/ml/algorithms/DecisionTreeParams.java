package it.alessandronatilla.analysis.ml.algorithms;

import java.io.Serializable;

public class DecisionTreeParams implements Serializable {
	private Integer classCount = 2;
	private Integer maxDepth = 30;
	private Integer bins = 32;
	private String impurityMeasure = "entropy";
	
	public DecisionTreeParams(){
		
	}

	public DecisionTreeParams(Integer classCount, Integer maxDepth, Integer bins, String impurityMeasure) {
		this.classCount = classCount;
		this.maxDepth = maxDepth;
		this.bins = bins;
		this.impurityMeasure = impurityMeasure;
	}

	public Integer getClassCount() {
		return classCount;
	}

	public void setClassCount(Integer classCount) {
		this.classCount = classCount;
	}

	public Integer getMaxDepth() {
		return maxDepth;
	}

	public void setMaxDepth(Integer maxDepth) {
		this.maxDepth = maxDepth;
	}

	public Integer getBins() {
		return bins;
	}

	public void setBins(Integer bins) {
		this.bins = bins;
	}

	public String getImpurityMeasure() {
		return impurityMeasure;
	}

	public void setImpurityMeasure(String impurityMeasure) {
		this.impurityMeasure = impurityMeasure;
	}

	@Override
	public String toString() {
		return "DecisionTreeParams [classCount=" + classCount + ", maxDepth="
				+ maxDepth + ", bins=" + bins + ", impurityMeasure="
				+ impurityMeasure + "]";
	}
	
	
}