package it.alessandronatilla.analysis.ml.algorithms;

import java.io.Serializable;

public class RandomForestParams implements Serializable {
	private Integer numClasses = 2;
	private Integer maxDepth = 30;
	private Integer bins = 32;
	private Integer numTrees = 5;
	private Integer seed = 12345;
	private String featureSubsetStrategy = "auto";
	private String impurity = "entropy";
	
	
	public RandomForestParams(){
		
	}
	
	
	public RandomForestParams(Integer numClasses, Integer maxDepth,
			Integer bins, Integer numTrees, Integer seed,
			String featureSubsetStrategy, String impurity) {
		super();
		this.numClasses = numClasses;
		this.maxDepth = maxDepth;
		this.bins = bins;
		this.numTrees = numTrees;
		this.seed = seed;
		this.featureSubsetStrategy = featureSubsetStrategy;
		this.impurity = impurity;
	}
	
	
	public Integer getNumClasses() {
		return numClasses;
	}
	
	public void setNumClasses(Integer numClasses) {
		this.numClasses = numClasses;
	}
	
	public Integer getMaxDepth() {
		return maxDepth;
	}
	
	public void setMaxDepth(Integer maxDepth) {
		this.maxDepth = maxDepth;
	}
	
	public Integer getBins() {
		return bins;
	}
	
	public void setBins(Integer bins) {
		this.bins = bins;
	}
	
	public Integer getNumTrees() {
		return numTrees;
	}
	
	public void setNumTrees(Integer numTrees) {
		this.numTrees = numTrees;
	}
	
	public Integer getSeed() {
		return seed;
	}
	
	public void setSeed(Integer seed) {
		this.seed = seed;
	}
	
	public String getFeatureSubsetStrategy() {
		return featureSubsetStrategy;
	}
	
	public void setFeatureSubsetStrategy(String featureSubsetStrategy) {
		this.featureSubsetStrategy = featureSubsetStrategy;
	}
	
	public String getImpurity() {
		return impurity;
	}
	
	public void setImpurity(String impurity) {
		this.impurity = impurity;
	}

	@Override
	public String toString() {
		return "RandomForestParams [numClasses=" + numClasses + ", maxDepth="
				+ maxDepth + ", bins=" + bins + ", numTrees=" + numTrees
				+ ", seed=" + seed + ", featureSubsetStrategy="
				+ featureSubsetStrategy + ", impurity=" + impurity + "]";
	}
	
}
