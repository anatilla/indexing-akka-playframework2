package it.alessandronatilla.analysis.ml.algorithms;

import it.alessandronatilla.analysis.matrix.DataPoint;
import it.alessandronatilla.analysis.ml.Algorithm;
import it.alessandronatilla.analysis.ml.ClassificationModel;
import it.alessandronatilla.analysis.spark.SparkProvider;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.evaluation.MulticlassMetrics;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.tree.RandomForest;
import org.apache.spark.mllib.tree.model.RandomForestModel;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RandomForestAlgorithm extends Algorithm<RandomForestParams> {

    @Override
    public ClassificationModel buildClassifier(RandomForestParams params) {
        return new RandomForestClassifier(params);
    }

    @Override
    public Class<RandomForestParams> getPojoParamClass() {
        return RandomForestParams.class;
    }

    private class RandomForestClassifier implements ClassificationModel {

        private RandomForestParams params;
        private RandomForestModel model;

        private RandomForestClassifier(RandomForestParams p) {
            this.params = p;
        }

        @Override
        public void train(JavaRDD<DataPoint> trainSetDD) {
            JavaRDD<LabeledPoint> trainSet = trainSetDD.map(DataPoint::getPoint);

            this.model = RandomForest.trainClassifier(trainSet,
                    this.params.getNumClasses(),
                    new HashMap<>(),
                    this.params.getNumTrees(),
                    this.params.getFeatureSubsetStrategy(),
                    this.params.getImpurity(),
                    this.params.getMaxDepth(),
                    this.params.getBins(),
                    this.params.getSeed()
            );
        }

        @Override
        public Map<String, Object> test(JavaRDD<DataPoint> testSet) {
            Map<String, Object> results = new HashMap<>();

            JavaRDD<Tuple2<Object, Object>> predictionAndLabels = testSet.map(
                    p -> {
                        Double prediction = model.predict(p.getPoint().features());
                        return new Tuple2<>(prediction, p.getPoint().label());
                    }
            );

            MulticlassMetrics metrics = new MulticlassMetrics(predictionAndLabels.rdd());
            results.put("precision", metrics.precision());
            results.put("recall", metrics.recall());
            results.put("f1-score", metrics.fMeasure());
            results.put("confusion_matrix", metrics.confusionMatrix().toString());
            List<Double> list = Arrays.stream(metrics.labels()).boxed().collect(Collectors.toList());

            list.stream().forEach(label -> {
                results.put("precision " + label, metrics.precision(label));
                results.put("recall " + label, metrics.recall(label));
                results.put("f1-score " + label, metrics.fMeasure(label));
            });

            return results;
        }

        @Override
        public void load(String fpath) throws IOException {
            if (!new File(fpath).exists()) {
                throw new IOException(fpath + " doesn't exists");
            }
            this.model = RandomForestModel.load(SparkProvider.getInstance().getContext().sc(), fpath);
        }

        @Override
        public void save(String fpath) {
            if (this.model != null) {
                this.model.save(SparkProvider.getInstance().getContext().sc(), fpath);
            }
        }

        @Override
        public Serializable getParams() {
            return this.params;
        }
    }

}
