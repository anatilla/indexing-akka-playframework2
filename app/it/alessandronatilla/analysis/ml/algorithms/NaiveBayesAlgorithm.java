package it.alessandronatilla.analysis.ml.algorithms;

import it.alessandronatilla.analysis.ml.Algorithm;
import it.alessandronatilla.analysis.ml.ClassificationModel;
import it.alessandronatilla.analysis.matrix.DataPoint;
import it.alessandronatilla.analysis.spark.SparkProvider;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.classification.NaiveBayes;
import org.apache.spark.mllib.classification.NaiveBayesModel;
import org.apache.spark.mllib.evaluation.MulticlassMetrics;
import org.apache.spark.mllib.regression.LabeledPoint;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class NaiveBayesAlgorithm extends Algorithm<NaiveBayesParams> {

    @Override
    public ClassificationModel buildClassifier(NaiveBayesParams params) {
        return new NaiveBayesClassifier(params);
    }

    @Override
    public Class<NaiveBayesParams> getPojoParamClass() {
        return NaiveBayesParams.class;
    }

    private class NaiveBayesClassifier implements ClassificationModel {

        private NaiveBayesParams params;
        private NaiveBayesModel model;

        private NaiveBayesClassifier(NaiveBayesParams params) {
            this.params = params;
        }

        @Override
        public void train(JavaRDD<DataPoint> trainSetDD) {
            JavaRDD<LabeledPoint> trainSet = trainSetDD.map(DataPoint::getPoint);
            this.model = NaiveBayes.train(trainSet.rdd());
        }


        @Override
        public Map<String, Object> test(JavaRDD<DataPoint> testSet) {
            Map<String, Object> results = new HashMap<>();

            JavaRDD<Tuple2<Object, Object>> predictionAndLabels = testSet.map(
                    p -> {
                        Double prediction = model.predict(p.getPoint().features());
                        return new Tuple2<>(prediction, p.getPoint().label());
                    }
            );


            MulticlassMetrics metrics = new MulticlassMetrics(predictionAndLabels.rdd());
            results.put("precision", metrics.precision());
            results.put("recall", metrics.recall());
            results.put("f1-score", metrics.fMeasure());
            results.put("confusion_matrix", metrics.confusionMatrix().toString());
            List<Double> list = Arrays.stream(metrics.labels()).boxed().collect(Collectors.toList());

            list.stream().forEach(label -> {
                results.put("precision " + label, metrics.precision(label));
                results.put("recall " + label, metrics.recall(label));
                results.put("f1-score " + label, metrics.fMeasure(label));
            });

            return results;
        }

        @Override
        public void load(String fpath) throws IOException {
            if (!new File(fpath).exists()) {
                throw new IOException(fpath + " doesn't exists");
            }
            this.model = NaiveBayesModel.load(SparkProvider.getInstance().getContext().sc(), fpath);
        }

        @Override
        public void save(String fpath) {
            if (this.model != null) {
                this.model.save(SparkProvider.getInstance().getContext().sc(), fpath);
            }
        }


        @Override
        public Serializable getParams() {
            return this.params;
        }

    }

}
