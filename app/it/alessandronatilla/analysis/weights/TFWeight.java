package it.alessandronatilla.analysis.weights;

import it.alessandronatilla.analysis.model.matrix.IndexedField;
import it.alessandronatilla.analysis.exceptions.IllegalDocumentIDException;
import it.alessandronatilla.analysis.exceptions.IllegalTermException;
import org.apache.lucene.index.IndexReader;

import java.util.Map;

/**
 * Created by alexander on 01/05/15.
 */
public class TFWeight extends Weight {
    public TFWeight(IndexReader reader, IndexedField field) {
        super(reader, field);
    }

    @Override
    public double local(String term, String documentID) throws IllegalTermException, IllegalDocumentIDException {
        if (!super.idMapping.containsKey(documentID))
            throw new IllegalArgumentException("document ID " + documentID + " doesn't exists");
        if (!super.termIdMap.containsKey(term))
            throw new IllegalArgumentException("term " + term + " doesn't exists");

        int docid = idMapping.get(documentID);
        return super.local_weights.get(new TermDocumentKey(term, docid));
    }

    @Override
    public double global(String term) throws IllegalTermException{
        if (!super.termIdMap.containsKey(term))
            throw new IllegalArgumentException("term " + term + " doesn't exists");

        return 1d;
    }

    @Override
    public double norm(String documentID) throws IllegalDocumentIDException{
        if (!super.idMapping.containsKey(documentID))
            throw new IllegalArgumentException("document ID " + documentID + " doesn't exists");
        return 1d;
    }

    @Override
    public Map getTermIdMap() {
        return super.getTermIdMap();
    }
}
