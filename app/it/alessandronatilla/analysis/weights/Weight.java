package it.alessandronatilla.analysis.weights;

import com.google.common.base.Objects;
import it.alessandronatilla.analysis.model.matrix.IndexedField;
import it.alessandronatilla.analysis.exceptions.IllegalDocumentIDException;
import it.alessandronatilla.analysis.exceptions.IllegalTermException;
import it.alessandronatilla.utils.JLogger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.*;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.search.similarities.TFIDFSimilarity;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexander on 01/05/15.
 */
public abstract class Weight {
    protected Map<String, Integer> termIdMap;
    protected IndexedField field;
    protected Map<String, Integer> idMapping;
    protected IndexReader reader;
    protected Map<TermDocumentKey, Double> local_weights; //tf
    protected Map<String, Double> global_weights; //idf

    public Weight(IndexReader reader, IndexedField field) {
        this.termIdMap = new HashMap<>();
        this.idMapping = new HashMap<>();
        this.global_weights = new HashMap<>();
        this.local_weights = new HashMap<>();
        this.field = field;
        this.reader = reader;
        try {
            this.prepareTermsIdMapping(reader);
            this.prepareLuceneDocumentsMapping();
            this.prepare();
        } catch (IOException e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());
        }

    }

    private void prepareLuceneDocumentsMapping() throws IOException {
        // mapping between lucene id and document own id
        for (int i = 0; i < reader.numDocs(); i++) {
            Document doc = reader.document(i);
            String own_id = doc.get("id");
            idMapping.put(own_id, i);
        }
    }

    private void prepareTermsIdMapping(IndexReader reader) throws IOException {
        int id = 0;
        System.out.println("READER: " + reader);
        Fields fields = MultiFields.getFields(reader);
        System.out.println("FIELDS: " + fields); // fields = null in production mode. PERCHè?
        Terms terms = fields.terms(field.name());
        TermsEnum itr = terms.iterator(null);
        BytesRef term = null;
        while ((term = itr.next()) != null) {
            String termText = term.utf8ToString();
            if (termIdMap.containsKey(termText))
                continue;
            termIdMap.put(termText, id++);
        }

    }

    public abstract double local(String term, String documentID) throws IllegalTermException, IllegalDocumentIDException;

    public abstract double global(String term) throws IllegalTermException;

    public abstract double norm(String documentID) throws IllegalDocumentIDException;

    public Map<String, Integer> getTermIdMap() {
        return termIdMap;
    }

    private void prepare() throws IOException {
        // compute tfidf scores
        TFIDFSimilarity tfidfSIM = new DefaultSimilarity();
        TermsEnum termsEnum = MultiFields.getTerms(this.reader, this.field.name()).iterator(
                null);
        BytesRef bytesRef;
        DocsEnum docsEnum = null;

        while ((bytesRef = termsEnum.next()) != null) {
            if (termsEnum.seekExact(bytesRef, false)) {
                String term = bytesRef.utf8ToString();
                Double tf = 0d;

                docsEnum = termsEnum.docs(null, null, DocsEnum.FLAG_FREQS);
                while (docsEnum.nextDoc() != DocIdSetIterator.NO_MORE_DOCS) {
                    tf = Double.valueOf(docsEnum.freq());
                    Float idf = tfidfSIM.idf(termsEnum.docFreq(),
                            this.reader.numDocs());
                    local_weights.put(new TermDocumentKey(term, docsEnum.docID()), tf);
                    global_weights.put(term, Double.valueOf(idf));
                }
            }
        }
    }

    public class TermDocumentKey {
        public String term;
        public Integer documentID;

        public TermDocumentKey(String term, Integer documentID) {
            this.term = term;
            this.documentID = documentID;
        }

        @Override
        public String toString() {
            return "TermDocumentKey{" +
                    "term='" + term + '\'' +
                    ", documentID=" + documentID +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TFIDFWeight.TermDocumentKey that = (TFIDFWeight.TermDocumentKey) o;
            return Objects.equal(term, that.term) &&
                    Objects.equal(documentID, that.documentID);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(term, documentID);
        }
    }
}
