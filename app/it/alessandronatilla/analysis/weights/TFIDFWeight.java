package it.alessandronatilla.analysis.weights;

import it.alessandronatilla.analysis.exceptions.IllegalDocumentIDException;
import it.alessandronatilla.analysis.exceptions.IllegalTermException;
import it.alessandronatilla.analysis.model.matrix.IndexedField;
import org.apache.lucene.index.IndexReader;

import java.util.Map;

/**
 * Created by alexander on 01/05/15.
 */

public class TFIDFWeight extends Weight {

    public TFIDFWeight(IndexReader ireader, IndexedField field) {
        super(ireader, field);
    }

    public double local(String term, String documentID) throws IllegalTermException, IllegalDocumentIDException {
        if (!super.idMapping.containsKey(documentID))
            throw new IllegalDocumentIDException("document ID " + documentID + " doesn't exists");
        if (!super.termIdMap.containsKey(term))
            throw new IllegalTermException("term " + term + " doesn't exists");
        int docid = idMapping.get(documentID);
        return local_weights.get(new TermDocumentKey(term, docid));
    }

    public double global(String term) throws IllegalTermException {
        if (!super.termIdMap.containsKey(term))
            throw new IllegalTermException("term " + term + " doesn't exists");
        return global_weights.get(term);
    }

    public double norm(String documentID) throws IllegalDocumentIDException {
        if (!super.idMapping.containsKey(documentID))
            throw new IllegalDocumentIDException("document ID " + documentID + " doesn't exists");
        return 1d;
    }

    @Override
    public Map getTermIdMap() {
        return super.getTermIdMap();
    }
}
