package it.alessandronatilla.analysis.weights;

import it.alessandronatilla.analysis.model.matrix.IndexedField;
import it.alessandronatilla.analysis.exceptions.IllegalDocumentIDException;
import it.alessandronatilla.analysis.exceptions.IllegalTermException;
import it.alessandronatilla.utils.JLogger;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.Map;

/**
 * Created by alexander on 01/05/15.
 *
 * @see "http://en.wikipedia.org/wiki/Okapi_BM25"
 */
public class BM25Weight extends Weight {

    private double k = 1.2d;
    private float avgDocumentLenght;
    private double b = 0.75d;

    public BM25Weight(IndexReader reader, IndexedField field) {
        super(reader, field);

        try {
            this.avgDocumentLenght = getAvgLength(reader, field.name());
        } catch (IOException e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());        }
    }

    public BM25Weight(IndexReader reader, IndexedField field, double k, double b) {
        super(reader, field);
        this.k = k;
        this.b = b;

        try {
            this.avgDocumentLenght = getAvgLength(reader, field.name());
        } catch (IOException e) {
            JLogger.getLogger(this.getClass().getName()).error(e.getMessage());        }
    }

    private static float getAvgLength(IndexReader reader, String field) throws IOException {
        long sum = 0l;

        TermsEnum termsEnum = MultiFields.getTerms(reader, field).iterator(
                null);
        BytesRef bytesRef;

        while ((bytesRef = termsEnum.next()) != null) {
            sum += termsEnum.totalTermFreq();
        }

        float avg = (float) sum / reader.numDocs();
        return avg;
    }

    @Override
    public double local(String term, String documentID) throws IllegalTermException, IllegalDocumentIDException {
        if (!super.idMapping.containsKey(documentID))
            throw new IllegalDocumentIDException("document ID " + documentID + " doesn't exists");
        if (!super.termIdMap.containsKey(term))
            throw new IllegalTermException("term " + term + " doesn't exists");

        int docid = idMapping.get(documentID);
        double tf = local_weights.get(new TermDocumentKey(term, docid));

        double numerator = (tf * (k + 1));
        double denominator = (tf + k * (1 - b + (b * (reader.numDocs() / avgDocumentLenght))));
        return (numerator / denominator);
    }

    @Override
    public double global(String term)throws IllegalTermException{
        if (!super.termIdMap.containsKey(term))
            throw new IllegalTermException("term " + term + " doesn't exists");
        return super.global_weights.get(term);

    }

    @Override
    public double norm(String documentID) throws IllegalDocumentIDException{
        if (!super.idMapping.containsKey(documentID))
            throw new IllegalDocumentIDException("document ID " + documentID + " doesn't exists");
        return 1d;
    }

    @Override
    public Map getTermIdMap() {
        return super.termIdMap;
    }
}
