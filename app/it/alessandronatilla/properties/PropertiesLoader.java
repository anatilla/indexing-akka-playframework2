package it.alessandronatilla.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by alexander on 4/16/15.
 */
public class PropertiesLoader {
    public static Properties load() throws IOException {

        String fname = "config.properties";
        return load(fname);
    }

    public static Properties load(String fpath) throws IOException {
        if (fpath.isEmpty()) throw new IllegalArgumentException("fpath is empty");

        InputStream input = PropertiesLoader.class.getClassLoader().getResourceAsStream(fpath);
        if (input == null) throw new IllegalArgumentException("cannot find config.properties file.");

        Properties props = new Properties();
        props.load(input);

        return props;
    }
}
