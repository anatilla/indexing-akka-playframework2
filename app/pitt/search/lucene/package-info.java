/**

Package containing some basic Lucene untilities for creating and
searching indexes, particularly bilingual indexes.

@author Dominic Widdows, in collaboration with Kathleen Ferraro and
the University of Pittsburgh.

*/

package pitt.search.lucene;
