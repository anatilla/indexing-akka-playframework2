name := "indexer-playframework2"

version := "1.0"

lazy val `indexer-playframework2` = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies += "com.github.dwhjames" % "play-cors_2.11" % "0.1.0"

libraryDependencies ++= Seq(javaJdbc, cache, javaWs,
  "org.apache.spark" % "spark-core_2.11" % "1.3.1",
  "org.apache.spark" % "spark-mllib_2.11" % "1.3.1",
  "commons-io" % "commons-io" % "2.4",
  "org.apache.opennlp" % "opennlp-tools" % "1.5.3",
  "edu.stanford.nlp" % "stanford-corenlp" % "1.3.0",
  "org.apache.lucene" % "lucene-snowball" % "3.0.3",
  "org.apache.lucene" % "lucene-core" % "4.3.1" % "provided",
  "org.apache.lucene" % "lucene-analyzers-common" % "4.3.1",
  "org.apache.lucene" % "lucene-queryparser" % "4.3.1",
  "org.apache.commons" % "commons-math3" % "3.4.1",
  "com.google.guava" % "guava" % "18.0",
  javaJpa,
  "org.hibernate" % "hibernate-entitymanager" % "3.6.10.Final",
  "org.postgresql" % "postgresql" % "9.4-1200-jdbc4"
)

unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/testModel")


